To install the package in editable mode:
python -m pip install --no-build-isolation -e .

C++ are compiled on their own once the package is installed:
cmake -B build -S test && cmake --build build -j 20 && cmake --build build --target test
