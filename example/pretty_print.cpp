#include <algorithm>
#include <iostream>
#include <ranges>
#include <vector>

#include <compass_cxx_utils/pretty_print.h>

int main() {
  using namespace compass::utils;
  pretty_print(iota(0, 3));
  pretty_print(iota(0, 20));
  Pretty_printer print;
  print(2.6);
  print(iota(0, 20));
  print("iotas: {", iota(0, 20), "}");
}
