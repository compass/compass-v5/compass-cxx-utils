#pragma once

#include <array>

namespace compass {
namespace utils {

// from: https://stackoverflow.com/a/57757301
namespace detail {
template <typename T, std::size_t... Is>
constexpr std::array<T, sizeof...(Is)>
create_array(T value, std::index_sequence<Is...>) {
  // cast Is to void to remove the warning: unused value
  return {{(static_cast<void>(Is), value)...}};
}
} // namespace detail

template <std::size_t N, typename T>
constexpr std::array<T, N> create_array(const T &value) {
  return detail::create_array(value, std::make_index_sequence<N>());
}

namespace array_operators {

template <typename T, std::size_t dim>
inline T sum(const std::array<T, dim> &a) {
  T result = 0;
  for (auto &&x : a)
    result += x;
  return result;
}

template <typename T, std::size_t dim>
inline T prod(const std::array<T, dim> &a) {
  T result = 1;
  for (auto &&x : a)
    result *= x;
  return result;
}

template <typename T, std::size_t dim>
inline std::array<T, dim> operator+(const std::array<T, dim> &a,
                                    const std::array<T, dim> &b) {
  std::array<T, dim> result;
  for (std::size_t i = 0; i < dim; ++i)
    result[i] = a[i] + b[i];
  return result;
}

template <typename T, std::size_t dim>
inline std::array<T, dim> &operator+=(std::array<T, dim> &a,
                                      const std::array<T, dim> &b) {
  for (std::size_t i = 0; i < dim; ++i)
    a[i] += b[i];
  return a;
}

template <typename T, std::size_t dim>
inline std::array<T, dim> operator-(const std::array<T, dim> &a,
                                    const std::array<T, dim> &b) {
  std::array<T, dim> result;
  for (std::size_t i = 0; i < dim; ++i)
    result[i] = a[i] - b[i];
  return result;
}

template <typename T, std::size_t dim>
inline std::array<T, dim> &operator-=(std::array<T, dim> &a,
                                      const std::array<T, dim> &b) {
  for (std::size_t i = 0; i < dim; ++i)
    a[i] -= b[i];
  return a;
}

template <typename T, std::size_t dim>
inline std::array<T, dim> operator*(const std::array<T, dim> &a,
                                    const std::array<T, dim> &b) {
  std::array<T, dim> result;
  for (std::size_t i = 0; i < dim; ++i)
    result[i] = a[i] * b[i];
  return result;
}

template <typename T, std::size_t dim>
inline std::array<T, dim> &operator*=(std::array<T, dim> &a,
                                      const std::array<T, dim> &b) {
  for (std::size_t i = 0; i < dim; ++i)
    a[i] *= b[i];
  return a;
}

template <typename T, std::size_t dim>
inline std::array<T, dim> operator/(const std::array<T, dim> &a,
                                    const std::array<T, dim> &b) {
  std::array<T, dim> result;
  for (std::size_t i = 0; i < dim; ++i)
    result[i] = a[i] / b[i];
  return result;
}

template <typename T, std::size_t dim>
inline std::array<T, dim> &operator/=(std::array<T, dim> &a,
                                      const std::array<T, dim> &b) {
  for (std::size_t i = 0; i < dim; ++i)
    a[i] /= b[i];
  return a;
}

template <typename T, std::size_t dim>
inline std::array<T, dim> &operator+=(std::array<T, dim> &a, const T &lambda) {
  for (auto &&x : a)
    x += lambda;
  return a;
}

template <typename T, std::size_t dim>
inline std::array<T, dim> &operator-=(std::array<T, dim> &a, const T &lambda) {
  for (auto &&x : a)
    x -= lambda;
  return a;
}

template <typename T, std::size_t dim>
inline std::array<T, dim> &operator*=(std::array<T, dim> &a, const T &lambda) {
  for (auto &&x : a)
    x *= lambda;
  return a;
}

template <typename T, std::size_t dim>
inline std::array<T, dim> &operator/=(std::array<T, dim> &a, const T &lambda) {
  for (auto &&x : a)
    x /= lambda;
  return a;
}

} // namespace array_operators

} // namespace utils
} // namespace compass
