#pragma once

#include "binary_operators.h"
#include "enum_tuple.h"

namespace compass {
namespace utils {
namespace binops {

struct add {
  static auto apply(const auto &a, const auto &b) { return a + b; }
};

struct substract {
  static auto apply(const auto &a, const auto &b) { return a - b; }
};

struct multiply {
  static auto apply(const auto &a, const auto &b) { return a * b; }
};

struct divide {
  static auto apply(const auto &a, const auto &b) { return a / b; }
};

} // namespace binops
} // namespace utils
} // namespace compass
