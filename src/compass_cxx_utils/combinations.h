#pragma once

#include <array>
#include <cassert>
#include <optional>

namespace compass {
namespace utils {

/** Generates all conbinations of r elements amongst n. */
template <int r, int n> struct combinations {
  struct iterator {
    struct Sentinel {};
    static constexpr auto sentinel = Sentinel{};
    std::array<int, r> value;
    iterator() {
      for (int k = 0; k < r; ++k) {
        value[k] = k;
      }
    }
    iterator &operator++() {
      std::optional<int> reset;
      for (int k = r - 1; k >= 0; --k) {
        ++value[k];
        if (value[k] < n)
          break;
        reset = k;
      }
      if (reset) {
        assert(*reset > 0 || r == 1);
        for (int k = std::max(1, *reset); k < r; ++k) {
          value[k] = value[k - 1] + 1;
        }
      }
      return *this;
    }
    const auto &operator*() const { return value; }
    bool operator!=(const Sentinel &) const { return ok(); }
    bool ok() const {
      if constexpr (r == 0) {
        return false;
      } else {
        return value[r - 1] != n;
      }
    }
  };
  static iterator begin() { return {}; }
  // will not be used
  static iterator::Sentinel end() { return iterator::sentinel; }
};

} // namespace utils
} // namespace compass
