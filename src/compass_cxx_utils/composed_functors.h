#pragma once

#include <type_traits>
#include <utility>

#include "binary_operators.h"

namespace compass {
namespace utils {

// naive expression template

template <typename Op, typename F, typename G> struct Composed_functor {
  F f;
  G g;
  Composed_functor() = delete;
  template <typename Tf, typename Tg>
  Composed_functor(Tf &&fv, Tg &&gv)
      : f{std::forward<Tf>(fv)}, g{std::forward<Tg>(gv)} {}
  template <typename... X> auto operator()(X &&...x) const {
    return Op::apply(f(std::forward<X>(x)...), g(std::forward<X>(x)...));
  }
};
template <typename Op, typename F, typename G>
inline auto composition(F &&f, G &&g) {
  return Composed_functor<Op, std::decay_t<F>, std::decay_t<G>>{
      std::forward<F>(f), std::forward<G>(g)};
}

} // namespace utils
} // namespace compass
