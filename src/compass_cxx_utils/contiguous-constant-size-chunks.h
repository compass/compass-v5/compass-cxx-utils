#pragma once

#include <algorithm>
#include <cassert>
#include <ranges>
#include <span>
#include <stdexcept>
#include <utility>
#include <vector>

#include <compass_cxx_utils/self-managed.h>

namespace compass {

namespace utils {

/** CCSC stands for Contiguous Constant Size Chunks. */
template <typename T, std::size_t cs>
struct CCSC_data : Self_managed_object<CCSC_data<T, cs>> {
  using item_type = T;
  static constexpr std::size_t chunk_size = cs;
  std::vector<T> items;
  // Chunks are already stored contiguously
#ifdef COMPASS_WITH_CPP23
  template <std::ranges::range R>
    requires(!std::ranges::range<std::ranges::range_value_t<R>>)
  CCSC_data(R &&r) : items{std::from_range, std::forward<R>(r)} {
    if (r.size() % cs != 0) {
      throw std::runtime_error("Unconsistent chunk size.");
    }
  }
#else
  template <std::ranges::range R>
    requires(!std::ranges::range<std::ranges::range_value_t<R>> &&
             std::constructible_from<std::vector<T>, R>)
  CCSC_data(R &&r) : items{std::forward<R>(r)} {
    if (r.size() % cs != 0) {
      throw std::runtime_error("Unconsistent chunk size.");
    }
  }
  template <std::ranges::range R>
    requires(!std::ranges::range<std::ranges::range_value_t<R>> &&
             !std::constructible_from<std::vector<T>, R>)
  CCSC_data(R &&r) {
    if (r.size() % cs != 0) {
      throw std::runtime_error("Unconsistent chunk size.");
    }
    items.reserve(r.size());
    std::ranges::copy(std::forward<R>(r), back_inserter(items));
  }
#endif
  // Chunks are range of ranges
  template <std::ranges::range R>
    requires(std::ranges::range<std::ranges::range_value_t<R>>)
  CCSC_data(R &&r) {
    if (!std::ranges::all_of(
            r, [](auto &&chunk) { return std::ranges::size(chunk) == cs; })) {
      throw std::runtime_error("Unconsistent chunk size.");
    }
    items.reserve(cs * std::ranges::size(r));
    for (auto &&chunk : r) {
      std::ranges::copy(chunk, back_inserter(items));
    }
  }
};

/** A vector of vector of items stored in a contiguous portion of memory.
 * The vector of items are called chunks in the following.
 * All chunks have the same size, known at compile time.
 * This makes this structure a basic 2D array.
 */
template <typename T, std::size_t cs>
struct Contiguous_constant_size_chunks : CCSC_data<T, cs>::handle {
private:
  using Data = CCSC_data<T, cs>;
  using Data::handle::data;

public:
  using item_type = T;
  static constexpr std::size_t chunk_size = cs;
  using value_type = std::span<item_type, chunk_size>;

  template <typename TT> struct base_iterator {
    using value_type = std::span<TT, cs>;
    using pointer = value_type *;
    using reference = value_type;
    // FIXME: must be defined to as signed type for ranges-v3
    //        even for forward iterators
    using difference_type = long;
    using iterator_category = std::forward_iterator_tag;
    TT *items = nullptr;
    reference operator*() const { return std::span<TT, cs>{items, cs}; }
    base_iterator<TT> &operator++() {
      items += cs;
      return *this;
    }
    base_iterator<TT> operator++(int) {
      base_iterator<TT> pre_increment{*this};
      items += cs;
      return pre_increment;
    }
    bool operator==(const base_iterator<TT> &other) const {
      return items == other.items;
    }
    bool operator!=(const base_iterator<TT> &other) const {
      return items != other.items;
    }
    bool operator<(const base_iterator<TT> &other) const {
      return items < other.items;
    }
    // Not necessary as long as this class is used as a forward iterator
    // difference_type operator-(const Counter &other) const {
    // }
  };

  using iterator = base_iterator<T>;
  using const_iterator = base_iterator<const T>;

  template <std::ranges::range R>
  Contiguous_constant_size_chunks(R &&r)
      : Data::handle{Data::make(std::forward<R>(r))} {
    assert(is_consistent());
  }

  bool is_consistent() const {
    if (!this) {
      return data->items.size() % chunk_size == 0;
    }
    return true;
  }

  // FIXME: C++23 wait for gcc 14 with "deducing this"
  // template <typename Self>
  // auto begin(this Self &&self)
  //     -> std::conditional_t<std::is_const_v<std::remove_reference_t<Self>>,
  //                           const_iterator, iterator> {
  //   if (self) {
  //     return {self.data->items.data()};
  //   }
  //   assert(false);
  //   return {};
  // }

  const_iterator begin() const {
    if (data) {
      return {data->items.data()};
    }
    assert(false);
    return {};
  }

  iterator begin() {
    if (data) {
      return {data->items.data()};
    }
    assert(false);
    return {};
  }

  // FIXME: C++23 wait for gcc 14 with "deducing this"
  // template <typename Self>
  // auto end(this Self &&self)
  //     -> std::conditional_t<std::is_const_v<std::remove_reference_t<Self>>,
  //                           const_iterator, iterator> {
  //   if (self) {
  //     return {self.data->items.data() + self.data->items.size()};
  //   }
  //   assert(false);
  //   return {};
  // }

  const_iterator end() const {
    if (data) {
      return {data->items.data() + data->items.size()};
    }
    assert(false);
    return {};
  }

  iterator end() {
    if (data) {
      return {data->items.data() + data->items.size()};
    }
    assert(false);
    return {};
  }

  std::size_t size() const {
    if (data) {
      assert(is_consistent());
      return data->items.size() / chunk_size;
    }
    assert(false);
    return 0;
  }

  std::size_t number_of_items() const {
    if (this) {
      return data->items.size();
    }
    assert(false);
    return 0;
  }

  // FIXME: C++23 wait for gcc 14 with "deducing this"
  // template <typename Self>
  // auto operator[](this Self &&self, std::integral auto k) {
  //   assert(std::cmp_greater_equal(k, 0));
  //   assert(std::cmp_less(k, self.size()));
  //   constexpr bool is_const = std::is_const_v<std::remove_reference_t<Self>>;
  //   using chunk =
  //       std::conditional_t<is_const, std::span<const T>, std::span<T>>;
  //   return chunk{self.data->items.data() + k * chunk_size, chunk_size};
  // }

  std::span<const T> operator[](std::integral auto k) const {
    assert(data);
    assert(std::cmp_greater_equal(k, 0));
    assert(std::cmp_less(k, size()));
    return {data->items.data() + k * chunk_size, chunk_size};
  }

  std::span<T> operator[](std::integral auto k) {
    assert(data);
    assert(std::cmp_greater_equal(k, 0));
    assert(std::cmp_less(k, size()));
    return {data->items.data() + k * chunk_size, chunk_size};
  }

  // FIXME: C++23 wait for gcc 14 with "deducing this"
  // template <typename Self> auto items(this Self &&self) {
  //   constexpr bool is_const = std::is_const_v<std::remove_reference_t<Self>>;
  //   using span = std::conditional_t<is_const, std::span<const T>,
  //   std::span<T>>; if (self) {
  //     const auto p = self.data->items.data();
  //     return span{p, p + self.data->items.size()};
  //   }
  //   assert(false);
  //   return span{};
  // }

  std::span<const T> items() const {
    if (data) {
      const auto p = data->items.data();
      return {p, p + data->items.size()};
    }
    assert(false);
    return {};
  }

  std::span<T> items() {
    if (data) {
      const auto p = data->items.data();
      return {p, p + data->items.size()};
    }
    assert(false);
    return {};
  }
};

} // namespace utils

} // namespace compass
