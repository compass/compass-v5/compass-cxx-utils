#pragma once

#include <algorithm>
#include <cassert>
#include <ranges>
#include <span>
#include <vector>

#include <compass_cxx_utils/self-managed.h>

namespace compass {

namespace utils {

/** CRSC stands for Contiguous Random Size Chunks. */
template <typename T> struct CRSC_data : Self_managed_object<CRSC_data<T>> {
  using item_type = T;
  std::vector<std::size_t> offsets;
  std::vector<T> items;
  // Chunks are range of ranges
  template <std::ranges::range R>
    requires(std::ranges::range<std::ranges::range_value_t<R>>)
  CRSC_data(R &&r) {
    const auto n = size(r);
    offsets.reserve(n + 1);
    offsets.push_back(0);
    for (auto &&chunk : r) {
      offsets.push_back(offsets.back() + size(chunk));
    }
    items.reserve(offsets.back());
    for (auto &&chunk : r) {
      std::ranges::copy(chunk, back_inserter(items));
    }
  }
};

/** A vector of vector of items stored in a contiguous portion of memory.
 * The vector of items are called chunks in the following.
 * There is no constraint on the size of chunks.
 * When items are indices, this gives the classical implementation
 * of Compressed Row/Column Storage */
template <typename T>
struct Contiguous_random_size_chunks : CRSC_data<T>::handle {

private:
  using Data = CRSC_data<T>;
  using Data::handle::data;

public:
  using item_type = T;
  using value_type = std::span<item_type, std::dynamic_extent>;

  template <typename TT> struct base_iterator {
    using value_type = std::span<TT, std::dynamic_extent>;
    using pointer = value_type *;
    using reference = value_type;
    // FIXME: must be defined to as signed type for ranges-v3
    //        even for forward iterators
    using difference_type = long;
    using iterator_category = std::forward_iterator_tag;
    const std::size_t *offset = nullptr;
    // It could be tempting to define items as a const pointer but this would
    // invalidate the default definition of copy constructors and assignement
    // operators
    TT *items = nullptr;
    reference operator*() const {
      return {items + *offset, items + *std::next(offset)};
    }
    base_iterator<TT> &operator++() {
      ++offset;
      return *this;
    }
    base_iterator<TT> operator++(int) {
      base_iterator<TT> pre_increment{*this};
      ++offset;
      return pre_increment;
    }
    bool operator==(const base_iterator<TT> &other) const {
      assert(items == other.items);
      return offset == other.offset;
    }
    bool operator!=(const base_iterator<TT> &other) const {
      assert(items == other.items);
      return offset != other.offset;
    }
    bool operator<(const base_iterator<TT> &other) const {
      assert(items == other.items);
      return offset < other.offset;
    }
    // Not necessary as long as this class is used as a forward iterator
    // difference_type operator-(const Counter &other) const {
    // }
  };

  using iterator = base_iterator<T>;
  using const_iterator = base_iterator<const T>;

  // Chunks are range of ranges
  template <std::ranges::range R>
    requires(std::ranges::range<std::ranges::range_value_t<R>>)
  Contiguous_random_size_chunks(R &&r)
      : Data::handle{Data::make(std::forward<R>(r))} {
    assert(is_consistent());
  }

  bool is_consistent() const {
    if (!this) {
      if (data->offsets.front() != 0)
        return false;
      if (data->offsets.back() != data->items.size())
        return false;
    }
    return true;
  }

  // FIXME: C++23 wait for gcc 14 with "deducing this"
  // template <typename Self>
  // auto begin(this Self &&self)
  //     -> std::conditional_t<std::is_const_v<std::remove_reference_t<Self>>,
  //                           const_iterator, iterator> {
  //   if (self) {
  //     return {self.data->offsets.data(), self.data->items.data()};
  //   }
  //   assert(false);
  //   return {};
  // }

  const_iterator begin() const {
    if (data) {
      return {data->offsets.data(), data->items.data()};
    }
    assert(false);
    return {};
  }

  iterator begin() {
    if (data) {
      return {data->offsets.data(), data->items.data()};
    }
    assert(false);
    return {};
  }

  // FIXME: C++23 wait for gcc 14 with "deducing this"
  // template <typename Self>
  // auto end(this Self &&self)
  //     -> std::conditional_t<std::is_const_v<std::remove_reference_t<Self>>,
  //                           const_iterator, iterator> {
  //   if (self) {
  //     return {&(self.data->offsets.back()), self.data->items.data()};
  //   }
  //   assert(false);
  //   return {};
  // }

  const_iterator end() const {
    if (data) {
      return {&(data->offsets.back()), data->items.data()};
    }
    assert(false);
    return {};
  }

  iterator end() {
    if (data) {
      return {&(data->offsets.back()), data->items.data()};
    }
    assert(false);
    return {};
  }

  std::size_t size() const {
    if (this) {
      assert(!data->offsets.empty());
      return data->offsets.size() - 1;
    }
    assert(false);
    return 0;
  }

  std::size_t number_of_items() const {
    if (this) {
      assert(!data->offsets.empty());
      assert(data->offsets.back() == data->items.size());
      return data->items.size();
    }
    assert(false);
    return 0;
  }

  // FIXME: C++23 wait for gcc 14 with "deducing this"
  // template <typename Self>
  // auto operator[](this Self &&self, std::integral auto k) {
  //   assert(std::cmp_greater_equal(k, 0));
  //   assert(std::cmp_less(k, self.size()));
  //   constexpr bool is_const = std::is_const_v<std::remove_reference_t<Self>>;
  //   using chunk =
  //       std::conditional_t<is_const, std::span<const T>, std::span<T>>;
  //   return chunk{self.data->items.data() + self.data->offsets[k],
  //                self.data->items.data() + self.data->offsets[k + 1]};
  // }

  std::span<const T> operator[](std::integral auto k) const {
    assert(data);
    assert(std::cmp_greater_equal(k, 0));
    assert(std::cmp_less(k, size()));
    return {data->items.data() + data->offsets[k],
            data->items.data() + data->offsets[k + 1]};
  }

  std::span<T> operator[](std::integral auto k) {
    assert(data);
    assert(std::cmp_greater_equal(k, 0));
    assert(std::cmp_less(k, size()));
    return {data->items.data() + data->offsets[k],
            data->items.data() + data->offsets[k + 1]};
  }

  // FIXME: C++23 wait for gcc 14 with "deducing this"
  // template <typename Self> auto items(this Self &&self) {
  //   constexpr bool is_const = std::is_const_v<std::remove_reference_t<Self>>;
  //   using span = std::conditional_t<is_const, std::span<const T>,
  //   std::span<T>>; if (self) {
  //     const auto p = self.data->items.data();
  //     return span{p, p + self.data->items.size()};
  //   }
  //   assert(false);
  //   return span{};
  // }

  std::span<const T> items() const {
    if (data) {
      const auto p = data->items.data();
      return {p, p + data->items.size()};
    }
    assert(false);
    return {};
  }

  std::span<T> items() {
    if (data) {
      const auto p = data->items.data();
      return {p, p + data->items.size()};
    }
    assert(false);
    return {};
  }
};

} // namespace utils

} // namespace compass
