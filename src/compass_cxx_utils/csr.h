#pragma once

#include <algorithm>
#include <concepts>

#include <compass_cxx_utils/contiguous-constant-size-chunks.h>
#include <compass_cxx_utils/contiguous-random-size-chunks.h>

namespace compass {

namespace utils {

/** An implementation of Compressed Sparse Row with varying underlying storage.
 */
template <std::integral index_t = std::size_t, int nnzpr = -1> struct CSR {

  using index_type = index_t;
  using shape_type = std::pair<index_type, index_type>;

  const index_type nb_rows, nb_cols;
  bool has_sorted_nz_cols;

  static constexpr bool constant_number_of_nonzeros_per_row = (nnzpr >= 0);

private:
  // The actual row storage with two implementations
  // depending on the number of non-zeros per row (nnzpr)
  using storage_type =
      std::conditional_t <
      nnzpr<0, Contiguous_random_size_chunks<index_type>,
            Contiguous_constant_size_chunks<index_type, nnzpr>>;
  storage_type nz_cols;

public:
  CSR() = delete;

  template <std::ranges::range R>
  CSR(const shape_type &shape, R &&r)
      : nb_rows{shape.first}, nb_cols{shape.second},
        nz_cols{std::forward<R>(r)} {
    has_sorted_nz_cols = check_if_sorted_nz_cols();
    assert(is_consistent());
  }

  auto row_indices() const { return std::views::iota((index_type)0, nb_rows); }

  auto col_indices() const { return std::views::iota((index_type)0, nb_cols); }

  bool is_valid_row(const std::integral auto i) const {
    return std::cmp_greater_equal(i, 0) && std::cmp_less(i, nb_rows);
  }

  bool is_valid_column(const std::integral auto j) const {
    return std::cmp_greater_equal(j, 0) && std::cmp_less(j, nb_cols);
  }

  bool check_if_sorted_nz_cols() const {
    return std::ranges::all_of(
        nz_cols, [](const auto &r) { return std::ranges::is_sorted(r); });
  }

  bool is_consistent() const {
    if (!nz_cols.is_consistent())
      return false;
    if (std::cmp_less_equal(nb_rows, 0))
      return false;
    if (std::cmp_less_equal(nb_cols, 0))
      return false;
    if (!std::cmp_equal(nb_rows, nz_cols.size()))
      return false;
    if (std::ranges::any_of(nz_cols.items(), [this](auto &&j) {
          return !this->is_valid_column(j);
        }))
      return false;
    if (has_sorted_nz_cols && !check_if_sorted_nz_cols())
      return false;
    return true;
  }

  // whatever storage_type chunk views are spans so that copy is cheap
  auto nonzeros(const std::integral auto i) const {
    assert(is_valid_row(i));
    return nz_cols[i];
  }

  const storage_type &nonzeros() const { return nz_cols; }

  // whatever storage_type return a view (span) over items so that copy is cheap
  auto all_nonzeros() const { return nz_cols.items(); }

  auto number_of_nonzeros() const { return nz_cols.number_of_items(); }

  bool is_nonzero(const std::integral auto i,
                  const std::integral auto j) const {
    assert(is_valid_row(i));
    assert(is_valid_column(j));
    auto nzi = nz_cols[i];
    if (has_sorted_nz_cols) {
      return std::ranges::binary_search(nzi, (index_type)j);
    } else {
#ifdef COMPASS_WITH_CPP23
      return std::ranges::contains(nzi, (index_type)j);
#else
      return std::find(begin(nzi), end(nzi), (index_type)j) != end(nzi);
#endif
    }
  }

  bool is_zero(const std::integral auto i, const std::integral auto j) const {
    return !is_nonzero(i, j);
  }
};

} // namespace utils
} // namespace compass
