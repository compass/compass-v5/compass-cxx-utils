#pragma once

#include <utility>

#include "binary_operators.h"
#include "composed_functors.h"
#include "enum_tuple.h"

namespace compass {
namespace utils {

namespace detail {

template <typename Tuple, typename... X> struct same_return_types {
  constexpr static bool value = false;
};

template <typename... Fs, typename... X>
  requires(sizeof...(Fs) > 0)
struct same_return_types<std::tuple<Fs...>, X...> {
  using tuple_type = std::tuple<Fs...>;
  constexpr static bool value = std::conjunction_v<std::is_same<
      std::invoke_result_t<std::tuple_element_t<0, tuple_type>, X...>,
      std::invoke_result_t<Fs, X...>>...>;
};

template <typename Tuple, typename... X>
inline constexpr bool same_return_types_v =
    same_return_types<std::decay_t<Tuple>, X...>::value;

template <typename T> struct _for;

template <std::size_t... i> struct _for<std::index_sequence<i...>> {

  template <typename Functors, typename... X>
  constexpr static auto evaluate_as_tuple(const Functors &F, X &&...x) {
    return std::make_tuple(std::get<i>(F)(std::forward<X>(x)...)...);
  }

  template <typename Functors, typename... X>
    requires((std::tuple_size_v < Functors >> 0) &&
             (same_return_types_v<Functors, X...>))
  constexpr static auto evaluate_as_array(const Functors &F, X &&...x) {
    using result_type =
        std::invoke_result_t<std::tuple_element_t<0, Functors>, X...>;
    return std::array<result_type, std::tuple_size_v<Functors>>{
        std::get<i>(F)(std::forward<X>(x)...)...};
  }

  // FT = Functor tuple
  template <typename BinOp, typename Ft1, typename Ft2>
    requires(std::tuple_size_v<std::decay_t<Ft1>> ==
             std::tuple_size_v<std::decay_t<Ft2>>)
  constexpr static auto compose(Ft1 &&t1, Ft2 &&t2) {
    return std::make_tuple(
        composition<BinOp>(std::get<i>(t1), std::get<i>(t2))...);
  }
};

} // namespace detail

template <Enum_sequence DefinitionSet, typename... Fs>
struct Enum_functors : Enum_tuple<DefinitionSet, Fs...> {
  using base = Enum_tuple<DefinitionSet, Fs...>;
  using definition_set = typename base::definition_set;
  using enum_type = typename base::enum_type;
  using tuple_type = typename base::tuple_type;
  using base::as_index;
  using base::elements; // functors
  template <typename... X>
  static constexpr bool same_return_types =
      detail::same_return_types_v<tuple_type, X...>;

  template <typename T>
    requires(std::is_constructible_v<tuple_type, T &&>)
  Enum_functors(T &&t) : base{std::forward<T>(t)} {}

  template <typename... Gs>
    requires(std::conjunction_v<std::is_constructible<Fs, Gs &&>...>)
  Enum_functors(Gs &&...gs) : base{std::make_tuple(std::forward<Gs>(gs)...)} {}

  template <typename... X>
    requires(!same_return_types<X...>)
  auto operator()(X &&...x) const {
    return detail::_for<std::index_sequence_for<Fs...>>::evaluate_as_tuple(
        elements, std::forward<X>(x)...);
  }

  template <typename... X>
    requires same_return_types<X...>
  auto operator()(X &&...x) const {
    return detail::_for<std::index_sequence_for<Fs...>>::evaluate_as_array(
        elements, std::forward<X>(x)...);
  }

  template <typename... X> auto evaluate_as_tuple(X &&...x) const {
    return detail::_for<std::index_sequence_for<Fs...>>::evaluate_as_tuple(
        elements, std::forward<X>(x)...);
  }

  template <typename... X>
    requires same_return_types<X...>
  auto evaluate_as_array(X &&...x) const {
    return detail::_for<std::index_sequence_for<Fs...>>::evaluate_as_array(
        elements, std::forward<X>(x)...);
  }

  // template <typename... Gs>
  // Enum_functors(Gs &&...gs) :
  // functors{std::forward_as_tuple(std::forward<Gs>(gs)...)} {}
  /** \todo requires EvaluationSet::is_included_in<DefinitionSet>()
   *        to be defined
   */
  template <typename BinOp, typename EvaluationSet, typename... X>
    requires(is_enum_set_v<EvaluationSet> && EvaluationSet::size > 0 &&
             std::is_same_v<enum_type, typename EvaluationSet::enum_type>)
  auto accumulate(X &&...x) const {
    if constexpr (EvaluationSet::size == 1) {
      return std::get<as_index(EvaluationSet::first)>(elements)(
          std::forward<X>(x)...);
    } else {
      return BinOp::apply(std::get<as_index(EvaluationSet::first)>(elements)(
                              std::forward<X>(x)...),
                          accumulate<BinOp, typename EvaluationSet::others>(
                              std::forward<X>(x)...));
    }
  }
  template <typename EvaluationSet = DefinitionSet, typename... X>
  auto sum(X &&...x) const {
    return accumulate<binops::add, EvaluationSet>(std::forward<X>(x)...);
  }

  template <enum_type e, typename... X> constexpr auto apply(X &&...x) const {
    return std::get<as_index(e)>(elements)(std::forward<X>(x)...);
  }

  template <base::underlying_type itest = 0, typename... X>
    requires same_return_types<X...>
  constexpr auto _apply(const typename base::underlying_type i, X &&...x) const
      -> std::invoke_result_t<std::tuple_element_t<0, tuple_type>, X...> {

    assert(std::cmp_less_equal(0, i) && std::cmp_less(i, definition_set::size));
    static_assert(std::cmp_less_equal(0, itest) &&
                  std::cmp_less_equal(itest, definition_set::size));

    if constexpr (itest >= definition_set::size) {
      assert(false);
    } else {
      if (i == itest) {
        return std::get<itest>(elements)(std::forward<X>(x)...);
      }
      return std::get<itest + 1>(elements)(std::forward<X>(x)...);
    }
  }

  template <typename... X>
    requires same_return_types<X...>
  constexpr auto apply(const enum_type e, X &&...x) const
      -> std::invoke_result_t<std::tuple_element_t<0, tuple_type>, X...> {
    using result_type =
        std::invoke_result_t<std::tuple_element_t<0, tuple_type>, X...>;
    if constexpr (definition_set::size == 0) {
      assert(false);
    }
    if constexpr (definition_set::size == 1) {
      assert(as_index(e) == 0);
      return std::get<0>(elements)(std::forward<X>(x)...);
    }
    if constexpr (definition_set::size == 2) {
      switch (as_index(e)) {
      case 0:
        return std::get<0>(elements)(std::forward<X>(x)...);
      case 1:
        return std::get<1>(elements)(std::forward<X>(x)...);
      default:
        assert(false);
      }
    }
    if constexpr (definition_set::size == 3) {
      switch (as_index(e)) {
      case 0:
        return std::get<0>(elements)(std::forward<X>(x)...);
      case 1:
        return std::get<1>(elements)(std::forward<X>(x)...);
      case 2:
        return std::get<2>(elements)(std::forward<X>(x)...);
      default:
        assert(false);
      }
    }
    if constexpr (definition_set::size == 4) {
      switch (as_index(e)) {
      case 0:
        return std::get<0>(elements)(std::forward<X>(x)...);
      case 1:
        return std::get<1>(elements)(std::forward<X>(x)...);
      case 2:
        return std::get<2>(elements)(std::forward<X>(x)...);
      case 3:
        return std::get<3>(elements)(std::forward<X>(x)...);
      default:
        assert(false);
      }
    }
    if constexpr (definition_set::size == 5) {
      switch (as_index(e)) {
      case 0:
        return std::get<0>(elements)(std::forward<X>(x)...);
      case 1:
        return std::get<1>(elements)(std::forward<X>(x)...);
      case 2:
        return std::get<2>(elements)(std::forward<X>(x)...);
      case 3:
        return std::get<3>(elements)(std::forward<X>(x)...);
      case 4:
        return std::get<4>(elements)(std::forward<X>(x)...);
      default:
        assert(false);
      }
    }
    if constexpr (definition_set::size > 5) {
      return _apply(as_index(e), std::forward<X>(x)...);
    }
    return result_type{};
  }
};

template <Enum_sequence DefinitionSet>
  requires(DefinitionSet::size > 0)
struct Enum_functors_factory {

  static constexpr auto size = DefinitionSet::size;

  template <typename... Fs>
    requires(size == sizeof...(Fs))
  static auto from_functors(Fs &&...fs) {
    return Enum_functors<DefinitionSet, std::decay_t<Fs>...>{fs...};
  }

  template <typename T> struct with;

  template <std::size_t... i>
    requires(sizeof...(i) == size)
  struct with<std::index_sequence<i...>> {
    template <typename Tuple>
      requires(std::tuple_size_v<std::decay_t<Tuple>> == size)
    static auto from_tuple(Tuple &&ft) {
      return from_functors(std::get<i>(std::forward<Tuple>(ft))...);
    }
  };

  template <typename Tuple>
    requires(size == std::tuple_size_v<std::decay_t<Tuple>>)
  static auto from_tuple(Tuple &&ft) {
    return with<std::make_index_sequence<size>>::from_tuple(
        std::forward<Tuple>(ft));
  }
};

template <typename T> struct is_enum_functors {
  constexpr static bool value = false;
};

template <Enum_sequence DefinitionSet, typename... Fs>
struct is_enum_functors<Enum_functors<DefinitionSet, Fs...>> {
  constexpr static bool value = true;
};

template <typename T>
inline constexpr bool is_enum_functors_v =
    is_enum_functors<std::decay_t<T>>::value;

template <typename F, typename G>
  requires(is_enum_functors_v<F> && is_enum_functors_v<G> &&
           std::is_same_v<typename std::decay_t<F>::definition_set,
                          typename std::decay_t<G>::definition_set>)
auto operator+(F &&f, G &&g) {
  using definition_set = typename std::decay_t<F>::definition_set;
  return Enum_functors_factory<definition_set>::from_tuple(
      detail::_for<std::make_index_sequence<definition_set::size>>::
          template compose<binops::add>(f.elements, g.elements));
}

template <typename F, typename G>
  requires(is_enum_functors_v<F> && is_enum_functors_v<G> &&
           std::is_same_v<typename std::decay_t<F>::definition_set,
                          typename std::decay_t<G>::definition_set>)
auto operator-(F &&f, G &&g) {
  using definition_set = typename std::decay_t<F>::definition_set;
  return Enum_functors_factory<definition_set>::from_tuple(
      detail::_for<std::make_index_sequence<definition_set::size>>::
          template compose<binops::substract>(f.elements, g.elements));
}

template <typename F, typename G>
  requires(is_enum_functors_v<F> && is_enum_functors_v<G> &&
           std::is_same_v<typename std::decay_t<F>::definition_set,
                          typename std::decay_t<G>::definition_set>)
auto operator*(F &&f, G &&g) {
  using definition_set = typename std::decay_t<F>::definition_set;
  return Enum_functors_factory<definition_set>::from_tuple(
      detail::_for<std::make_index_sequence<definition_set::size>>::
          template compose<binops::multiply>(f.elements, g.elements));
}

template <typename F, typename G>
  requires(is_enum_functors_v<F> && is_enum_functors_v<G> &&
           std::is_same_v<typename std::decay_t<F>::definition_set,
                          typename std::decay_t<G>::definition_set>)
auto operator/(F &&f, G &&g) {
  using definition_set = typename std::decay_t<F>::definition_set;
  return Enum_functors_factory<definition_set>::from_tuple(
      detail::_for<std::make_index_sequence<definition_set::size>>::
          template compose<binops::divide>(f.elements, g.elements));
}

} // namespace utils
} // namespace compass
