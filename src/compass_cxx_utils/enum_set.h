#pragma once

#include <array>
#include <concepts>
#include <type_traits>
#include <utility>

namespace compass {
namespace utils {

template <typename EnumClass>
  requires(std::is_enum_v<EnumClass>)
struct Enum_set_base {
  using enum_type = EnumClass;
  using underlying_type = std::underlying_type_t<EnumClass>;
  constexpr static underlying_type to_underlying(enum_type enum_value) {
    return static_cast<underlying_type>(enum_value);
  }
};

template <typename EnumClass, EnumClass... ei> struct Enum_set;

template <typename EnumClass, EnumClass e0, EnumClass... ei>
struct Enum_set<EnumClass, e0, ei...> : Enum_set_base<EnumClass> {
  using Enum_set_base<EnumClass>::to_underlying;
  static constexpr std::size_t size = sizeof...(ei) + 1;
  static constexpr auto array = std::to_array({e0, ei...});
  static constexpr EnumClass first = e0;
  using others = Enum_set<EnumClass, ei...>;
};

template <typename EnumClass>
struct Enum_set<EnumClass> : Enum_set_base<EnumClass> {
  static constexpr std::size_t size = 0;
};

template <typename T> struct is_enum_set {
  constexpr static bool value = false;
};

template <typename EnumClass, EnumClass... ei>
struct is_enum_set<Enum_set<EnumClass, ei...>> {
  constexpr static bool value = true;
};

template <typename T>
inline constexpr bool is_enum_set_v = is_enum_set<std::decay_t<T>>::value;

template <typename ES, typename Seq> struct is_sequence_equivalent {
  constexpr static bool value = false;
};

template <typename EnumClass, EnumClass... e, std::size_t... n>
struct is_sequence_equivalent<Enum_set<EnumClass, e...>,
                              std::index_sequence<n...>> {
  constexpr static bool value =
      sizeof...(n) == sizeof...(e) &&
      std::conjunction_v<std::bool_constant<
          Enum_set<EnumClass, e...>::to_underlying(e) == n>...>;
};

template <typename ES, typename Seq>
inline constexpr bool is_sequence_equivalent_v =
    is_sequence_equivalent<ES, Seq>::value;

template <typename E>
concept Enum_sequence =
    is_enum_set_v<E> &&
    is_sequence_equivalent_v<E, std::make_index_sequence<E::size>>;

template <typename ES>
  requires Enum_sequence<ES>
constexpr auto index(const typename ES::enum_type e) {
  return ES::to_underlying(e);
}

} // namespace utils
} // namespace compass
