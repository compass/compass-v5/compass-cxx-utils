#pragma once

#include <cassert>
#include <tuple>

#include "enum_set.h"

namespace compass {
namespace utils {

/** Definition set enums must be convertible to a contiguous sequence of
 * std::size_t. */
template <Enum_sequence DefinitionSet, typename... Ts>
  requires(sizeof...(Ts) > 0 && sizeof...(Ts) == DefinitionSet::size)
struct Enum_tuple {
  using definition_set = DefinitionSet;
  using enum_type = typename DefinitionSet::enum_type;
  using underlying_type = typename DefinitionSet::underlying_type;
  using tuple_type = std::tuple<Ts...>;
  template <enum_type e>
  using element_type =
      std::tuple_element_t<DefinitionSet::to_underlying(e), tuple_type>;
  static constexpr auto as_index = definition_set::to_underlying;

  tuple_type elements;

  template <enum_type e> constexpr const auto &get() const {
    return std::get<as_index(e)>(elements);
  }
};

} // namespace utils
} // namespace compass
