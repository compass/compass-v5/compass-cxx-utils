#pragma once

#include <type_traits>
#include <utility>

namespace compass {

namespace meta {

namespace index_sequence {

template <std::size_t n, typename IS> struct prepend;

template <std::size_t n, std::size_t... nk>
struct prepend<n, std::index_sequence<nk...>> {
  using type = std::index_sequence<n, nk...>;
};

template <std::size_t n, typename IS>
using prepend_t = typename prepend<n, IS>::type;

template <typename IS, std::size_t n> struct append;

template <std::size_t n, std::size_t... nk>
struct append<std::index_sequence<nk...>, n> {
  using type = std::index_sequence<nk..., n>;
};

template <typename IS, std::size_t n>
using append_t = typename append<IS, n>::type;

template <std::size_t i, typename IS> struct keep;

template <std::size_t i, std::size_t n, std::size_t... nk>
  requires(i >= 1 + sizeof...(nk))
struct keep<i, std::index_sequence<n, nk...>> {
  using type = std::index_sequence<n, nk...>;
};

template <std::size_t i, std::size_t n, std::size_t... nk>
  requires(i > 0 && i < 1 + sizeof...(nk))
struct keep<i, std::index_sequence<n, nk...>> {
  using type =
      prepend_t<n, typename keep<i - 1, std::index_sequence<nk...>>::type>;
};

template <std::size_t... nk> struct keep<0, std::index_sequence<nk...>> {
  using type = std::index_sequence<>;
};

template <std::size_t i, typename IS> using keep_t = typename keep<i, IS>::type;

template <std::size_t i, typename IS> struct drop;

template <std::size_t i, std::size_t... nk>
struct drop<i, std::index_sequence<nk...>> {
  using type =
      std::conditional_t<i >= sizeof...(nk), std::index_sequence<>,
                         keep_t<sizeof...(nk) - i, std::index_sequence<nk...>>>;
};

template <std::size_t i, typename IS> using drop_t = drop<i, IS>::type;

template <std::size_t n, std::size_t i, typename IS> struct fill;

template <std::size_t n, std::size_t i, std::size_t... nk>
  requires(n > 0)
struct fill<n, i, std::index_sequence<nk...>> {
  using type =
      append_t<typename fill<n - 1, i, std::index_sequence<nk...>>::type, i>;
};

template <std::size_t i, std::size_t... nk>
struct fill<0, i, std::index_sequence<nk...>> {
  using type = std::index_sequence<nk...>;
};

template <std::size_t n, std::size_t i, typename IS>
using fill_t = typename fill<n, i, IS>::type;

template <std::size_t i, typename IS> struct index;

template <std::size_t i, std::size_t n, std::size_t... nk>
  requires(i > 0 && i <= sizeof...(nk))
struct index<i, std::index_sequence<n, nk...>> {
  static constexpr std::size_t value =
      index<i - 1, std::index_sequence<nk...>>::value;
};

template <std::size_t n, std::size_t... nk>
struct index<0, std::index_sequence<n, nk...>> {
  static constexpr std::size_t value = n;
};

template <std::size_t i, typename IS>
constexpr inline std::size_t index_v = index<i, IS>::value;

template <typename IS> struct len;

template <std::size_t... nk> struct len<std::index_sequence<nk...>> {
  static constexpr std::size_t value = sizeof...(nk);
};

template <typename IS> constexpr inline std::size_t len_v = len<IS>::value;

template <typename IS>
constexpr inline std::size_t last_v = index_v<len_v<IS> - 1, IS>;

template <typename IS, typename IS_from, typename IS_to> struct next;

template <typename IS, typename IS_from, typename IS_to>
  requires(len_v<IS> == len_v<IS_from> && len_v<IS> == len_v<IS_to>)
struct next<IS, IS_from, IS_to> {
  using type = std::conditional_t<
      last_v<IS> + 1 < last_v<IS_to>, append_t<drop_t<1, IS>, last_v<IS> + 1>,
      append_t<typename next<drop_t<1, IS>, drop_t<1, IS_from>,
                             drop_t<1, IS_to>>::type,
               last_v<IS_from>>>;
};

template <>
struct next<std::index_sequence<>, std::index_sequence<>,
            std::index_sequence<>> {
  using type = std::index_sequence<>;
};

template <typename IS, typename IS_from, typename IS_to>
using next_t = typename next<IS, IS_from, IS_to>::type;

template <typename IS, typename IS_from, typename IS_to>
inline constexpr bool stop_iteration_v =
    std::is_same_v<next_t<IS, IS_from, IS_to>, IS_from>;

template <typename IS_from, typename IS_to> struct Iterator {
  template <typename IS> using next = next_t<IS, IS_from, IS_to>;
  template <typename IS>
  static inline constexpr bool stop = stop_iteration_v<IS, IS_from, IS_to>;
  template <typename IS> static inline constexpr bool has_next = !stop<IS>;
};

} // namespace index_sequence

} // namespace meta

} // namespace compass
