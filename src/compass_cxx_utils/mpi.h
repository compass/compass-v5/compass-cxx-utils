#pragma once

#include <cassert>
#include <memory>
#include <numeric>
#include <ranges>
#include <span>
#include <stdexcept>
#include <type_traits>
#include <vector>

#include <mpi.h>

#include <compass_cxx_utils/self-managed.h>

namespace compass {

namespace mpi {

/** The initial design was to have

\code{.cpp}
template <typename T> struct mpi_datatype;
\endcode

with partial specialization:

\code{.cpp}
template <> struct mpi_datatype<int> {
  static constexpr MPI_Datatype value = MPI_INT;
};
\endcode

The specilization below cannot use constexpr because the actual
value is implementation dependant and may only be known at link time
cf. https://github.com/open-mpi/ompi/issues/10017

*/

template <typename... Ts> struct Type_list {
  template <typename T> static constexpr bool contains() {
    return std::disjunction_v<std::is_same<T, Ts>...>;
  }
};

#ifdef _WIN32
// FIXME: to make it work on windows
using Handled_types = Type_list<int, unsigned long, double, std::size_t>;
#else
using Handled_types = Type_list<int, unsigned long, double>;
#endif

template <typename T>
inline constexpr bool sent_as_bytes = !Handled_types::contains<T>();

template <typename T> inline MPI_Datatype datatype() noexcept {
#ifdef _WIN32
  static_assert(sizeof(std::size_t) == sizeof(unsigned long long));
#endif //  _WIN32

  if constexpr (std::is_same_v<T, int>) {
    assert(!sent_as_bytes<T>);
    return MPI_INT;
  } else if constexpr (std::is_same_v<T, unsigned long>) {
    assert(!sent_as_bytes<T>);
    return MPI_UNSIGNED_LONG;
  } else if constexpr (std::is_same_v<T, double>) {
    assert(!sent_as_bytes<T>);
    return MPI_DOUBLE;
  }
#ifdef _WIN32
  // FIXME: to make it work on windows
  else if constexpr (std::is_same_v<T, std::size_t>) {
    assert(!sent_as_bytes<T>);
    return MPI_UNSIGNED_LONG_LONG;
  }
#endif //  _WIN32
  assert(sent_as_bytes<T>);
  return MPI_DATATYPE_NULL;
}

inline bool is_null(const MPI_Datatype dt) { return dt == MPI_DATATYPE_NULL; }

struct Request {
  MPI_Request request;
  operator MPI_Request *() { return &request; }
  bool wait() {
    auto err = MPI_Wait(&request, MPI_STATUS_IGNORE);
    assert(err == MPI_SUCCESS);
    return err == MPI_SUCCESS;
  }
};

struct Requests {
  const std::size_t nb_requests;
  std::unique_ptr<MPI_Request[]> requests;
  Requests(const std::size_t n)
      : nb_requests{n}, requests{new MPI_Request[n]} {}
  MPI_Request *operator[](std::size_t k) const {
    assert(k < nb_requests);
    return &requests[k];
  }
  bool wait(std::size_t k) const {
    assert(k < nb_requests);
    auto err = MPI_Wait(&requests[k], MPI_STATUS_IGNORE);
    assert(err == MPI_SUCCESS);
    return err == MPI_SUCCESS;
  }
  bool wait_all() const {
    auto err = MPI_Waitall(nb_requests, requests.get(), MPI_STATUS_IGNORE);
    assert(err == MPI_SUCCESS);
    return err == MPI_SUCCESS;
  }
};

std::unique_ptr<MPI_Status[]> statuses;

struct Communicator {
  using rank_type = int;
  using tag_type = int;

private:
  const MPI_Comm comm = MPI_COMM_WORLD;
  rank_type _size = -1;
  rank_type _rank = -1;
  mutable tag_type tag = 0;
  mutable MPI_Status status;
  void init() {
    MPI_Comm_size(comm, &_size);
    MPI_Comm_rank(comm, &_rank);
  }
  template <typename T> static auto buffer_datatype() noexcept {
    if constexpr (sent_as_bytes<T>) {
      assert(is_null(datatype<T>()));
      return MPI_BYTE;
    } else {
      return datatype<T>();
    }
  }
  template <typename T> static auto buffer_size(const std::size_t &n) noexcept {
    // FIXME: the requirements that std::is_standard_layout_v is excessive here
    //        but is a guarantee for the following reinterpret_cast operations
    //        https://en.cppreference.com/w/cpp/types/is_standard_layout
    static_assert(std::is_standard_layout_v<T>);
    if constexpr (sent_as_bytes<T>) {
      return n * sizeof(T);
    } else {
      return n;
    }
  }

  template <typename T>
  void send(const rank_type receiver, const T *p, const std::size_t &n) const {
    assert(p);
    assert(is_communication_possible(_rank, receiver));
    MPI_Send(p, buffer_size<T>(n), buffer_datatype<T>(), receiver, tag, comm);
  }
  template <typename T>
  void send(const rank_type receiver, const std::vector<T> &v) const {
    const std::size_t n = v.size();
    send(receiver, n);
    send(receiver, v.data(), n);
  }
  template <typename T> void send(const rank_type receiver, const T &t) const {
    assert(!sent_as_bytes<T>);
    assert(is_communication_possible(_rank, receiver));
    MPI_Send(&t, 1, datatype<T>(), receiver, tag, comm);
  }

  template <typename T>
  void receive(const rank_type sender, T *p, const std::size_t &n) const {
    assert(p);
    assert(is_communication_possible(sender, _rank));
    MPI_Recv(p, buffer_size<T>(n), buffer_datatype<T>(), sender, tag, comm,
             &status);
  }
  template <typename T>
  void receive(const rank_type sender, std::vector<T> &v) const {
    const auto n = receive<std::size_t>(sender);
    v.resize(n);
    receive(sender, v.data(), n);
  }
  template <typename T> void receive(const rank_type sender, T &t) const {
    assert(!sent_as_bytes<T>);
    assert(is_communication_possible(sender, _rank));
    MPI_Recv(&t, 1, datatype<T>(), sender, tag, comm, &status);
  }
  template <typename T> T receive(const rank_type sender) const {
    T t{};
    receive(sender, t);
    return t;
  }

  template <typename T>
  void isend(const rank_type receiver, const T *p, const std::size_t &n,
             MPI_Request *request) const {
    assert(p);
    assert(is_communication_possible(_rank, receiver));
    MPI_Isend(p, buffer_size<T>(n), buffer_datatype<T>(), receiver, tag, comm,
              request);
  }
  template <typename T>
  void ireceive(const rank_type sender, T *p, const std::size_t &n,
                MPI_Request *request) const {
    assert(p);
    assert(is_communication_possible(sender, _rank));
    MPI_Irecv(p, buffer_size<T>(n), buffer_datatype<T>(), sender, tag, comm,
              request);
  }

  bool needs_communication(const rank_type sender,
                           const rank_type receiver) const noexcept {
    assert(is_communication_possible(sender, receiver));
    if ((_rank != sender) && (_rank != receiver)) {
      return false;
    }
    if (sender == receiver) {
      return false;
    }
    return true;
  }
  /** exchange with current tag */
  template <typename T>
  void atomic_exchange(const rank_type sender, const rank_type receiver,
                       T &t) const {
    if (_rank == sender)
      send(receiver, t);
    if (_rank == receiver)
      receive(sender, t);
  }

public:
  Communicator() { init(); }
  auto size() const { return _size; }
  auto rank() const { return _rank; }
  void barrier() const {
#ifndef NDEBUG
    auto result =
#endif
        MPI_Barrier(comm);
    assert(result == MPI_SUCCESS);
  }
  bool is_ok() const { return _size > 0 && _rank >= 0 && _rank < _size; }
  bool is_communicator_id_ok(const rank_type id) const {
    return id >= 0 && id < _size;
  }
  bool is_communication_possible(const rank_type sender,
                                 const rank_type receiver) const {
    return is_ok() && is_communicator_id_ok(sender) &&
           is_communicator_id_ok(receiver);
  }
  auto new_communication_context() const {
    ++tag;
#ifndef NDEBUG
    // all procs must increase tag
    barrier();
#endif
    return tag;
  }
  template <typename T>
  void exchange(const rank_type sender, const rank_type receiver, T *p,
                const std::size_t n) const {
    new_communication_context(); // all ranks must increase tag
    if (!needs_communication(sender, receiver))
      return;
    if (_rank == sender)
      send(receiver, p, n);
    if (_rank == receiver)
      receive(sender, p, n);
  }
  template <typename T, std::size_t n>
  void exchange(const rank_type sender, const rank_type receiver,
                T (&a)[n]) const {
    exchange(sender, receiver, &a[0], n);
  }
  template <typename T>
  void exchange(const rank_type sender, const rank_type receiver, T &t) const {
    new_communication_context(); // all ranks must increase tag
    if (!needs_communication(sender, receiver))
      return;
    atomic_exchange(sender, receiver, t);
  }

  /** to be called on sender with a corresponding call to reveive_part on
   * receivers */
  template <std::ranges::range Containers>
  auto scatter(Containers &parts,
               const bool preserve_parts = std::is_const_v<Containers>) const {
    assert(preserve_parts or !std::is_const_v<Containers>);
    assert(parts.size() == _size);
    new_communication_context(); // receiver tags are increased by receive_part
    // FIXME: i should be an id in communicator group
    auto p = begin(parts);
    auto own_part = p;
    for (rank_type receiver = 0; receiver != _size; ++receiver) {
      if (receiver == _rank) {
        own_part = p;
      } else {
        send(receiver, *p);
      }
      ++p;
    }
    using Part_type = typename std::decay_t<Containers>::value_type;
    using T = typename Part_type::value_type;
    std::vector<T> v;
    if (preserve_parts) {
      v = *own_part;
    } else {
      v = std::move(*own_part);
      if constexpr (!std::is_const_v<Containers>) {
        parts.clear();
      }
    }
    return v;
  }
  /** to be called on receivers after a scatter on sender */
  template <typename T>
  void receive_part(const rank_type sender, std::vector<T> &v) const {
    new_communication_context(); // sender tag is increased by scatter
    receive(sender, v);
  }
  friend class Synchronization_map;
};

struct Synchronization_map
    : compass::utils::Self_managed_object<Synchronization_map> {

  struct info {
    Communicator::rank_type proc;
    std::size_t n;
  };

  const Communicator &communicator;
  std::vector<info> parts;
  std::size_t nb_sent_parts;
  std::size_t _sending_buffer_size;
  Requests requests;

  template <std::ranges::range Sent, std::ranges::range Received>
  Synchronization_map(const Communicator &comm, Sent &&sent,
                      Received &&received)
      : communicator{comm}, requests{sent.size() + received.size()} {
    parts.reserve(sent.size() + received.size());
    for (auto &&s : sent)
      parts.emplace_back(s);
    nb_sent_parts = parts.size();
    assert(nb_sent_parts == sent.size());
    for (auto &&r : received)
      parts.emplace_back(r);
    auto nb_items = [](auto &&r) {
      return std::accumulate(
          r.begin(), r.end(), (std::size_t)0,
          [](const std::size_t &n, const info &i) { return n + i.n; });
    };
    _sending_buffer_size = nb_items(sent);
    assert(nb_items(sent) + nb_items(received) == buffer_size());
  }

  std::size_t sending_buffer_size() const noexcept {
    assert(parts.cbegin() + nb_sent_parts <= parts.cend());
    return _sending_buffer_size;
  }

  std::size_t reception_buffer_size() const noexcept {
    assert(parts.cbegin() + nb_sent_parts <= parts.cend());
    return std::accumulate(
        parts.begin() + nb_sent_parts, parts.end(), (std::size_t)0,
        [](const std::size_t &n, const info &part) { return n + part.n; });
  }

  std::size_t buffer_size() const noexcept {
    return sending_buffer_size() + reception_buffer_size();
  }

  /** All procs must call synchronize to have the same communication context. */
  void
  synchronize(const std::ranges::contiguous_range auto &&sending_buffer,
              std::ranges::contiguous_range auto &&reception_buffer) const {
    assert(sending_buffer.size() == sending_buffer_size());
    assert(reception_buffer.size() == reception_buffer_size());
    communicator.new_communication_context();
    std::size_t rk = 0; // request
    auto pinfo = parts.cbegin();
    {
      // first send data
      auto p = sending_buffer.data();
      for (; pinfo != parts.cbegin() + nb_sent_parts; ++pinfo) {
        auto &&[proc, n] = *pinfo;
        communicator.isend(proc, p, n, requests[rk]);
        p += n;
        ++rk;
      }
    }
    {
      // then receive data
      auto q = reception_buffer.data();
      for (; pinfo != parts.cend(); ++pinfo) {
        auto &&[proc, n] = *pinfo;
        communicator.ireceive(proc, q, n, requests[rk]);
        q += n;
        ++rk;
      }
    }
    // wait for all requests to complete
    requests.wait_all();
  }

  /** All procs must call synchronize to have the same communication context. */
  void synchronize(std::ranges::contiguous_range auto &&buffer) const {
    assert(buffer.size() == buffer_size());
    synchronize(std::ranges::subrange(buffer.cbegin(),
                                      buffer.cbegin() + _sending_buffer_size),
                std::ranges::subrange(buffer.begin() + _sending_buffer_size,
                                      buffer.end()));
  }
};

struct Synchronizer {
  using info = Synchronization_map::info;
  template <std::ranges::range Sent, std::ranges::range Received>
  Synchronizer(const Communicator &comm, Sent &&sent, Received &&received) {
    smap = Synchronization_map::make(comm, std::forward<Sent>(sent),
                                     std::forward<Received>(received));
  }
  auto buffer_size() const noexcept {
    assert(smap);
    return smap->buffer_size();
  }
  void
  synchronize(const std::ranges::contiguous_range auto &&sending_buffer,
              std::ranges::contiguous_range auto &&reception_buffer) const {
    assert(smap);
    smap->synchronize(sending_buffer, reception_buffer);
  }
  void synchronize(std::ranges::contiguous_range auto &&buffer) const {
    assert(smap);
    smap->synchronize(buffer);
  }

private:
  Synchronization_map::handle smap;
};

} // namespace mpi

} // namespace compass
