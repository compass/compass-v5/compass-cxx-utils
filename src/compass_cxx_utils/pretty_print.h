#pragma once

#include <algorithm>
#include <cassert>
#include <iostream>
#include <ranges>
#include <tuple>

namespace compass {
namespace utils {

struct Pretty_printer {
  std::ostream &os = std::cout;
  const char *sep = " ";
  const char *endl = "\n";
  int nbmax = 15;
  int nbmin = -1;
  const char *range_sep = ",";
  template <std::ranges::range R>
  void process(const R &r)
    requires(!(std::is_same_v<std::decay_t<R>, std::string> ||
               std::is_same_v<std::decay_t<R>, char *>))
  {
    using namespace std::ranges;
    using namespace std::views;
    if (std::ranges::size(r) == 0)
      return;
    if (nbmin < 0)
      nbmin = int(nbmax / 3) + 1;
    assert(nbmin > 0);
    assert(2 * nbmin <= nbmax);
    const auto n = ssize(r);
    if (n < nbmax) {
      for (auto &&x : r | take(n - 1))
        os << sep << x << range_sep;
    } else {
      for (auto &&x : r | take(nbmin))
        os << sep << x << range_sep;
      os << sep << "..." << range_sep << sep;
      if (nbmin > 1) {
        for (auto &&x : r | drop(n - nbmin) | take(nbmin - 1))
          os << sep << x << range_sep;
      }
    }
    // FIXME: check concept reversible range or slide by 2
    for (auto &&x : r | drop(n - 1))
      os << sep << x;
  }
  template <std::size_t... i, typename... Ts>
  void _process_tuple(std::index_sequence<i...> &&,
                      const std::tuple<Ts...> &t) {
    static_assert(sizeof...(i) == sizeof...(Ts));
    if constexpr (sizeof...(i) != 0) {
      process_all(std::get<i>(t)...);
    }
  }
  template <typename... Ts> void process(const std::tuple<Ts...> &t) {
    _process_tuple(std::index_sequence_for<Ts...>{}, t);
  }
  void process(const auto &t) { os << t; }
  template <typename... Ts> void process_all(const Ts &...ts);
  template <typename T> void process_all(const T &t) { process(t); }
  template <typename T, typename... Ts>
  void process_all(const T &t, const Ts &...ts) {
    process(t);
    if constexpr (sizeof...(Ts) != 0) {
      os << sep;
      process_all(ts...);
    }
  }
  template <typename... Ts> void operator()(const Ts &...ts) {
    if constexpr (sizeof...(Ts) != 0) {
      process_all(ts...);
    }
    os << endl;
  }
};

inline void pretty_print(auto &&t, std::ostream &os = std::cout,
                         const char *sep = " ", const char *endl = "\n",
                         const int nbmax = 15, int nbmin = -1) {
  Pretty_printer{os, sep, endl, nbmax, nbmin}(std::forward<decltype(t)>(t));
}

} // namespace utils
} // namespace compass
