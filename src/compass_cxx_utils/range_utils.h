#pragma once

#include <ranges>

namespace compass {
namespace utils {

template <std::ranges::range R> R &sort_and_unique(R &r) {
  sort(r.begin(), r.end());
  auto last = unique(r.begin(), r.end());
  r.erase(last, r.end());
  return r;
}

} // namespace utils
} // namespace compass
