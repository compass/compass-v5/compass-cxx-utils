#pragma once

#include <atomic>
#include <cassert>

namespace compass {
namespace utils {

/** A python-like object which is counting its own references
and delete itseld when the last reference is lost.
cf. https://nanobind.readthedocs.io/en/latest/ownership_adv.html
*/
template <typename T> struct Self_managed_object {

  std::size_t use_count() const noexcept { return _ref_count; }

  /** handles do not have iterator interface because they references a single
   * object */
  struct handle {
    handle() noexcept : data{nullptr} {}
    handle(const handle &other) noexcept : data{other.data} { inc_ref(); }
    handle(T *p) : data{p} {
      static_assert(std::derived_from<T, Self_managed_object>); // enforces CRP
      inc_ref();
    }

    handle(handle &&other) noexcept : data{other.data} { other.data = nullptr; }
    handle &operator=(const handle &other) noexcept {
      dec_ref();
      data = other.data;
      inc_ref();
      return *this;
    }
    handle &operator=(handle &&other) noexcept {
      dec_ref();
      data = other.data;
      other.data = nullptr;
      return *this;
    }
    handle &operator=(T *p) noexcept {
      static_assert(std::derived_from<T, Self_managed_object>); // enforces CRP
      dec_ref();
      data = p;
      return *this;
    }
    ~handle() { dec_ref(); }
    explicit operator bool() const noexcept { return data != nullptr; }
    operator T *() const noexcept { return data; }
    T *raw() const noexcept { return data; }
    operator T &() const noexcept {
      assert(data);
      return *data;
    }
    bool operator==(const handle &other) const noexcept {
      return data == other.data;
    }
    bool operator<(const handle &other) const noexcept {
      return data < other.data;
    }
    T &operator*() const noexcept {
      assert(data);
      return *data;
    }
    T *operator->() const noexcept {
      assert(data);
      return data;
    }

  protected:
    T *data;

  private:
    void dec_ref() {
      if (data)
        data->dec_ref();
    }
    void inc_ref() {
      if (data)
        data->inc_ref();
    }
  };

  template <typename... Args> static handle make(Args &&...args) {
    static_assert(std::is_base_of_v<Self_managed_object, T>);
    return handle{new T{std::forward<Args>(args)...}};
  }

protected:
  Self_managed_object() = default;

private:
  Self_managed_object(const Self_managed_object &) = delete;
  Self_managed_object(Self_managed_object &&) = delete;
  Self_managed_object &operator=(const Self_managed_object &) = delete;
  Self_managed_object &operator=(Self_managed_object &&) = delete;

  mutable std::atomic<std::size_t> _ref_count{0};

  void inc_ref() const noexcept {
    static_assert(std::derived_from<T, Self_managed_object>); // enforces CRP
    ++_ref_count;
  }

  void dec_ref() const noexcept {
    static_assert(std::derived_from<T, Self_managed_object>); // enforces CRP
    if (--_ref_count == 0)
      delete (T *)this;
  }
};

} // namespace utils
} // namespace compass
