#pragma once

#include <array>
#include <cassert>
#include <concepts>
#include <functional>
#include <utility>

namespace compass {
namespace utils {

namespace detail {

template <typename T> struct Product;

template <> struct Product<std::index_sequence<>> {
  static constexpr std::size_t value = 1;
};

template <std::size_t n, std::size_t... nk>
struct Product<std::index_sequence<n, nk...>> {
  static constexpr std::size_t value =
      n * Product<std::index_sequence<nk...>>::value;
};

template <std::size_t... nk>
inline constexpr std::size_t product =
    Product<std::index_sequence<nk...>>::value;

template <std::size_t n, std::size_t... nk> struct Offset {
  template <std::integral... Ik>
  constexpr std::size_t operator()(std::integral auto i, Ik... ik) const
    requires(sizeof...(nk) >= sizeof...(ik))
  {
    assert(std::cmp_greater_equal(i, 0));
    assert(std::cmp_less(i, n));
    if constexpr (sizeof...(ik) == 0) {
      return ((std::size_t)i) * product<nk...>;
    } else {
      return ((std::size_t)i) * product<nk...> + Offset<nk...>{}(ik...);
    }
  }
};

template <typename IS1, typename IS2> struct Is_same_sequence {
  static constexpr bool value = false;
};

template <std::size_t... ni, std::size_t... nj>
  requires(sizeof...(ni) == sizeof...(nj))
struct Is_same_sequence<std::index_sequence<ni...>,
                        std::index_sequence<nj...>> {
  static constexpr bool value =
      std::make_tuple(ni...) == std::make_tuple(nj...);
};

template <typename T1, typename T2>
inline constexpr bool is_same_sequence_v = Is_same_sequence<T1, T2>::value;

template <std::size_t n, typename T> struct Drop;

template <std::size_t n, template <class, std::size_t...> typename T,
          typename A, std::size_t n0, std::size_t... nk>
  requires(n >= 1 && n <= sizeof...(nk))
struct Drop<n, T<A, n0, nk...>> {
  using type = typename Drop<n - 1, T<A, nk...>>::type;
};

template <template <class, std::size_t...> typename T, typename A,
          std::size_t... nk>
struct Drop<0, T<A, nk...>> {
  using type = T<A, nk...>;
};

template <std::size_t n, typename T> using drop_t = typename Drop<n, T>::type;

} // namespace detail

template <typename T, std::size_t... nk> struct Tensor {

  using value_type = T;

private:
  static constexpr detail::Offset<nk...> offset = {};
  template <std::size_t... nl>
  static constexpr bool same_shape =
      detail::is_same_sequence_v<std::index_sequence<nk...>,
                                 std::index_sequence<nl...>>;

  std::array<T, detail::product<nk...>> buffer;

public:
  static constexpr std::size_t ndim() { return sizeof...(nk); }

  static constexpr std::size_t size() { return detail::product<nk...>; }

  Tensor() = default;

  Tensor(const Tensor<T, nk...> &) = default;

  Tensor(Tensor<T, nk...> &&) = default;

  Tensor<T, nk...> &operator=(const Tensor<T, nk...> &) = default;

  Tensor<T, nk...> &operator=(Tensor<T, nk...> &&) = default;

  template <typename TT, std::size_t n>
    requires(same_shape<n> &&
             std::is_assignable_v<std::add_lvalue_reference_t<T>, TT>)
  Tensor(const TT (&a)[n]) {
    *this = a;
  }

  template <typename TT, std::size_t n>
    requires(same_shape<n> &&
             std::is_assignable_v<std::add_lvalue_reference_t<T>, TT>)
  Tensor<T, nk...> &operator=(const TT (&a)[n]) {
    for (std::size_t i = 0; i < n; ++i) {
      buffer[i] = a[i];
    }
    return *this;
  }

  template <typename TT, std::size_t ni, std::size_t nj>
    requires(same_shape<ni, nj> &&
             std::is_assignable_v<std::add_lvalue_reference_t<T>, TT>)
  Tensor(const TT (&a)[ni][nj]) {
    *this = a;
  }

  template <typename TT, std::size_t ni, std::size_t nj>
    requires(same_shape<ni, nj> &&
             std::is_assignable_v<std::add_lvalue_reference_t<T>, TT>)
  Tensor<T, nk...> &operator=(const TT (&a)[ni][nj]) {
    for (std::size_t i = 0; i < ni; ++i) {
      for (std::size_t j = 0; j < nj; ++j) {
        buffer[i * nj + j] = a[i][j];
      }
    }
    return *this;
  }

  template <typename TT>
    requires(!std::is_same_v<T, TT> &&
             std::is_assignable_v<std::add_lvalue_reference_t<T>, TT>)
  Tensor(const Tensor<TT, nk...> &t) {
    *this = t;
  }

  template <typename TT>
    requires(!std::is_same_v<T, TT> &&
             std::is_assignable_v<std::add_lvalue_reference_t<T>, TT>)
  Tensor<T, nk...> &operator=(const Tensor<TT, nk...> &t) {
    auto p = begin();
    for (auto &&x : t) {
      *p = x;
      ++p;
    }
    return *this;
  }

  template <typename Scalar>
    requires(std::is_assignable_v<std::add_lvalue_reference_t<T>, Scalar>)
  Tensor(const Scalar &y) {
    *this = y;
  }

  template <typename Scalar>
    requires(std::is_assignable_v<std::add_lvalue_reference_t<T>, Scalar>)
  Tensor<T, nk...> &operator=(const Scalar &y) {
    for (auto &&x : *this) {
      x = y;
    }
    return *this;
  }

  template <typename TT, std::size_t n>
    requires(n == Tensor<T, nk...>::size() &&
             std::is_assignable_v<std::add_lvalue_reference_t<T>, TT>)
  Tensor(const std::array<TT, n> &a) {
    *this = a;
  }

  template <typename TT, std::size_t n>
    requires(n == Tensor<T, nk...>::size() &&
             std::is_assignable_v<std::add_lvalue_reference_t<T>, TT>)
  Tensor<T, nk...> &operator=(const std::array<TT, n> &a) {
    for (std::size_t i = 0; i < n; ++i) {
      buffer[i] = a[i];
    }
    return *this;
  }

  template <std::integral... Ik>
  T &operator()(Ik... ik)
    requires(sizeof...(nk) == sizeof...(ik))
  {
    return buffer[offset(ik...)];
  }

  template <std::integral... Ik>
  const T &operator()(Ik... ik) const
    requires(sizeof...(nk) == sizeof...(ik))
  {
    return buffer[offset(ik...)];
  }

  template <std::integral... Ik>
    requires(sizeof...(Ik) < sizeof...(nk))
  detail::drop_t<sizeof...(Ik), Tensor<T, nk...>> &operator()(Ik... ik) {
    using return_t = detail::drop_t<sizeof...(Ik), Tensor<T, nk...>>;
    return *((return_t *)(buffer.data() + offset(ik...)));
  }

  template <std::integral... Ik>
    requires(sizeof...(Ik) < sizeof...(nk))
  const detail::drop_t<sizeof...(Ik), Tensor<T, nk...>> &
  operator()(Ik... ik) const {
    using return_t = detail::drop_t<sizeof...(Ik), Tensor<T, nk...>>;
    return *((const return_t *)(buffer.data() + offset(ik...)));
  }

  static constexpr std::array<std::size_t, sizeof...(nk)> shape() {
    return {{nk...}};
  }

  T *data() { return buffer.data(); }

  const T *data() const { return buffer.data(); }

  T *begin() { return data(); }

  const T *begin() const { return data(); }

  T *end() { return data() + size(); }

  const T *end() const { return data() + size(); }

  template <typename TT>
  Tensor<T, nk...> &operator+=(const Tensor<TT, nk...> &other) {
    auto p = begin();
    for (auto &&x : other) {
      (*p) += x;
      ++p;
    }
    return *this;
  }

  template <typename TT>
  Tensor<T, nk...> &operator-=(const Tensor<TT, nk...> &other) {
    auto p = begin();
    for (auto &&x : other) {
      (*p) -= x;
      ++p;
    }
    return *this;
  }

  template <typename TT>
  Tensor<T, nk...> &operator*=(const Tensor<TT, nk...> &other) {
    auto p = begin();
    for (auto &&x : other) {
      (*p) *= x;
      ++p;
    }
    return *this;
  }

  template <typename TT>
  Tensor<T, nk...> &operator/=(const Tensor<TT, nk...> &other) {
    auto p = begin();
    for (auto &&x : other) {
      (*p) /= x;
      ++p;
    }
    return *this;
  }

  template <typename Scalar>
    requires(std::is_arithmetic_v<Scalar>)
  Tensor<T, nk...> &operator+=(const Scalar &y) {
    for (auto &x : *this) {
      x += y;
    }
    return *this;
  }

  template <typename Scalar>
    requires(std::is_arithmetic_v<Scalar>)
  Tensor<T, nk...> &operator-=(const Scalar &y) {
    for (auto &x : *this) {
      x -= y;
    }
    return *this;
  }

  template <typename Scalar>
    requires(std::is_arithmetic_v<Scalar>)
  Tensor<T, nk...> &operator*=(const Scalar &y) {
    for (auto &x : *this) {
      x *= y;
    }
    return *this;
  }

  template <typename Scalar>
    requires(std::is_arithmetic_v<Scalar>)
  Tensor<T, nk...> &operator/=(const Scalar &y) {
    for (auto &x : *this) {
      x /= y;
    }
    return *this;
  }

  Tensor<T, nk...> operator-() const {
    Tensor<T, nk...> result;
    auto p = result.data();
    for (auto &x : *this) {
      *p = -x;
      ++p;
    }
    return result;
  }
};

template <typename T1, typename T2, std::size_t... nk>
inline auto operator+(const Tensor<T1, nk...> &t1,
                      const Tensor<T2, nk...> &t2) {
  using T = std::common_type_t<T1, T2>;
  Tensor<T, nk...> result{t1};
  result += t2;
  return result;
}

template <typename T1, typename T2, std::size_t... nk>
inline auto operator-(const Tensor<T1, nk...> &t1,
                      const Tensor<T2, nk...> &t2) {
  using T = std::common_type_t<T1, T2>;
  Tensor<T, nk...> result{t1};
  result -= t2;
  return result;
}

template <typename T1, typename T2, std::size_t... nk>
inline auto operator*(const Tensor<T1, nk...> &t1,
                      const Tensor<T2, nk...> &t2) {
  using T = std::common_type_t<T1, T2>;
  Tensor<T, nk...> result{t1};
  result *= t2;
  return result;
}

template <typename T1, typename T2, std::size_t... nk>
inline auto operator/(const Tensor<T1, nk...> &t1,
                      const Tensor<T2, nk...> &t2) {
  using T = std::common_type_t<T1, T2>;
  Tensor<T, nk...> result{t1};
  result /= t2;
  return result;
}

template <typename T, typename Scalar, std::size_t... nk>
  requires(std::is_arithmetic_v<Scalar>)
inline auto operator+(const Tensor<T, nk...> &t, const Scalar &x) {
  Tensor<T, nk...> result{t};
  result += x;
  return result;
}

template <typename T, typename Scalar, std::size_t... nk>
  requires(std::is_arithmetic_v<Scalar>)
inline auto operator+(const Scalar &x, const Tensor<T, nk...> &t) {
  Tensor<T, nk...> result{x};
  result += t;
  return result;
}

template <typename T, typename Scalar, std::size_t... nk>
  requires(std::is_arithmetic_v<Scalar>)
inline auto operator-(const Tensor<T, nk...> &t, const Scalar &x) {
  Tensor<T, nk...> result{t};
  result -= x;
  return result;
}

template <typename T, typename Scalar, std::size_t... nk>
  requires(std::is_arithmetic_v<Scalar>)
inline auto operator-(const Scalar &x, const Tensor<T, nk...> &t) {
  Tensor<T, nk...> result{x};
  result -= t;
  return result;
}

template <typename T, typename Scalar, std::size_t... nk>
  requires(std::is_arithmetic_v<Scalar>)
inline auto operator*(const Tensor<T, nk...> &t, const Scalar &x) {
  Tensor<T, nk...> result{t};
  result *= x;
  return result;
}

template <typename T, typename Scalar, std::size_t... nk>
  requires(std::is_arithmetic_v<Scalar>)
inline auto operator*(const Scalar &x, const Tensor<T, nk...> &t) {
  Tensor<T, nk...> result{x};
  result *= t;
  return result;
}

template <typename T, typename Scalar, std::size_t... nk>
  requires(std::is_arithmetic_v<Scalar>)
inline auto operator/(const Tensor<T, nk...> &t, const Scalar &x) {
  Tensor<T, nk...> result{t};
  result /= x;
  return result;
}

template <typename T, typename Scalar, std::size_t... nk>
  requires(std::is_arithmetic_v<Scalar>)
inline auto operator/(const Scalar &x, const Tensor<T, nk...> &t) {
  Tensor<T, nk...> result{x};
  result /= t;
  return result;
}

// deduction guides

template <typename T, std::size_t n> Tensor(const T (&a)[n]) -> Tensor<T, n>;

template <typename T, std::size_t ni, std::size_t nj>
Tensor(const T (&a)[ni][nj]) -> Tensor<T, ni, nj>;

namespace helper {

template <typename T> struct Is_tensor {
  static constexpr bool value = false;
};

template <typename T, std::size_t... n>
struct Is_tensor<compass::utils::Tensor<T, n...>> {
  static constexpr bool value = true;
};

} // namespace helper

template <typename T>
inline static constexpr bool is_tensor = helper::Is_tensor<T>::value;

} // namespace utils
} // namespace compass
