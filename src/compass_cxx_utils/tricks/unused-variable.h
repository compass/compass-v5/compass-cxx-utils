#pragma once

// trick used to silent unused variable warning
// this DOES NOT check if the variable is actually unused
#define UNUSED_VARIABLE(var)                                                   \
  {                                                                            \
    (void)(var);                                                               \
  }
