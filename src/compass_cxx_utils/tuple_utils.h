#pragma once

#include <tuple>

namespace compass {
namespace utils {

// from: https://stackoverflow.com/a/38894158

namespace detail {

template <std::size_t I, typename T> struct tuple_n {
  template <typename... Args>
  using type = typename tuple_n<I - 1, T>::template type<T, Args...>;
};

template <typename T> struct tuple_n<0, T> {
  template <typename... Args> using type = std::tuple<Args...>;
};

template <typename> struct Tuple_factory;

template <std::size_t... i> struct Tuple_factory<std::index_sequence<i...>> {
  template <typename T>
  static constexpr auto apply(const std::array<T, sizeof...(i)> &a) {
    return std::make_tuple(T{a[i]}...);
  }
};

template <typename T> struct Is_homogeneous_tuple {
  static constexpr bool value = false;
};

template <typename T, typename... Ts>
struct Is_homogeneous_tuple<std::tuple<T, Ts...>> {
  static constexpr bool value = std::conjunction_v<std::is_same<T, Ts>...>;
};

} // namespace detail

template <std::size_t I, typename T>
using tuple_of = typename detail::tuple_n<I, T>::template type<>;

template <typename T, std::size_t n>
constexpr auto as_tuple(const std::array<T, n> &a) {
  return detail::Tuple_factory<std::make_index_sequence<n>>::apply(a);
}

template <typename T>
inline constexpr bool is_homogeneous_tuple =
    detail::Is_homogeneous_tuple<std::decay_t<T>>::value;

} // namespace utils
} // namespace compass
