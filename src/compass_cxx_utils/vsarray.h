#pragma once

#include <algorithm>
#include <array>
#include <cassert>
#include <cstdint>
#include <initializer_list>
#include <limits>
#include <ranges>
#include <utility>

namespace compass {
namespace utils {

template <typename T, std::size_t n> struct vsarray {
  using size_type = std::uint8_t;
  size_type used;
  std::array<T, n> buffer;
  constexpr vsarray() : used{0}, buffer{} {
    static_assert(n <= std::numeric_limits<size_type>::max());
  }
  constexpr vsarray(const vsarray &) = default;
  vsarray &operator=(const vsarray &) = default;
  constexpr vsarray(vsarray &&) = delete;
  vsarray &operator=(vsarray &&) = delete;
  constexpr vsarray(const std::initializer_list<T> &a)
      : used{static_cast<size_type>(a.size())}, buffer{}
  // should be useless with C++20 https://stackoverflow.com/a/71085832
  {
    assert(a.size() <= n);
    std::copy(a.begin(), a.end(), buffer.data());
  }
  // template <std::size_t m>
  // requires(m<=n)
  // constexpr vsarray(const std::array<T, m>& a) :
  //  used{m},
  //  buffer{} // should be useless with C++20
  //  https://stackoverflow.com/a/71085832
  // {
  //             std::copy(a.begin(), a.end(), buffer.data());
  // }
  template <std::ranges::range R>
  constexpr vsarray(const R &r)
      : used{static_cast<size_type>(std::ranges::size(r))}, buffer{}
  // should be useless with C++20 https://stackoverflow.com/a/71085832
  {
    assert(std::ranges::size(r) <= n);
    std::ranges::copy(r, buffer.data());
  }
  T &operator[](const std::size_t i) {
    assert(i < used);
    return buffer[i];
  }
  const T &operator[](const std::size_t i) const {
    assert(i < used);
    return buffer[i];
  }
  auto size() const { return used; }
  T *data() { return buffer.data(); }
  T *data() const { return buffer.data(); }
  T *begin() { return buffer.data(); }
  T *end() { return buffer.data() + used; }
  const T *begin() const { return buffer.data(); }
  const T *end() const { return buffer.data() + used; }
};

} // namespace utils
} // namespace compass
