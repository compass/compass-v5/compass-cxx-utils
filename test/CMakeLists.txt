cmake_minimum_required(VERSION 3.24)
cmake_policy(VERSION 3.24)

project(tests LANGUAGES CXX)

include(CTest)

execute_process(
  COMMAND compass use-compass-file
  OUTPUT_VARIABLE USE_COMPASS
  OUTPUT_STRIP_TRAILING_WHITESPACE)

include(${USE_COMPASS})

find_package(compass-cxx-utils REQUIRED)
find_package(Eigen3 REQUIRED)

compass_add_cpp_catch2tests(
  FILES
  chunk.cpp
  csr.cpp
  ccsc-crsc.cpp
  combinations.cpp
  enumerate.cpp
  enum_sets.cpp
  enum_set_index.cpp
  ipow.cpp
  iset-prototype.cpp
  meta.cpp
  owning_view.cpp
  pretty_print.cpp
  repeat.cpp
  self-managed.cpp
  slide.cpp
  stride.cpp
  tensor.cpp
  tuple_utils.cpp
  vsarray.cpp
  zip.cpp
  PRIVATE
  compass-cxx-utils
  Eigen3::Eigen)

# target_link_libraries(test-tensor PRIVATE Eigen3::Eigen)

compass_add_mpi_tests(FILES mpi.cpp PRIVATE compass-cxx-utils)
