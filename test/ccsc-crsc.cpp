#include <algorithm>
#include <catch2/catch_test_macros.hpp>
#include <ranges>
#include <vector>

#include <compass_cxx_utils/contiguous-constant-size-chunks.h>
#include <compass_cxx_utils/contiguous-random-size-chunks.h>

template <int n> auto fill_crsc() {
  using std::ranges::copy;
  using std::views::iota;
  using namespace compass::utils;
  using CRSC = Contiguous_random_size_chunks<int>;
  using ivec = std::vector<int>;

  int l = 1;
  int i = 0;

  std::vector<ivec> chunks;
  for (int k = 0; k < n; ++k) {
    ivec chunk;
    chunk.reserve(l);
    copy(iota(i, i + l), back_inserter(chunk));
    chunks.emplace_back(move(chunk));
    i += l;
    ++l;
  }

  return CRSC{chunks};
}

template <int n>
  requires(n % 2 == 1)
auto fill_ccsc() {
  using std::views::iota;
  using namespace compass::utils;
  using CCSC = Contiguous_constant_size_chunks<int, (n + 1) / 2>;

  return CCSC{iota(0, n * (n + 1) / 2)};
}

template <int n> void check(auto &C) {
  using std::ranges::equal;
  using std::views::iota;

  STATIC_REQUIRE(std::ranges::input_range<decltype(C)>);
  STATIC_REQUIRE(std::ranges::range<decltype(C)>);

  CHECK(std::cmp_equal(C.size(), n));
  CHECK(C.number_of_items() == n * (n + 1) / 2);
  CHECK(equal(C.items(), iota(0, n * (n + 1) / 2)));

  std::size_t k = 0;
  for (auto &&chunk : C) {
    CHECK(equal(chunk, C[k]));
    ++k;
  }

  if constexpr (!std::is_const_v<std::remove_reference_t<decltype(C)>>) {
    for (auto &&chunk : C) {
      for (auto &&i : chunk)
        ++i;
    }
    int i = 1;
    for (auto &&chunk : C) {
      CHECK(equal(chunk, iota(i, i + (int)chunk.size())));
      i += (int)chunk.size();
    }
  }
}

void check_crsc(const auto &C) {

  std::size_t k = 0;
  for (auto &&chunk : C) {
    CHECK(chunk.size() == k + 1);
    ++k;
  }
}

TEST_CASE("ccsc crsc") {
  constexpr int n = 7;

  auto crsc = fill_crsc<n>();
  check<n>(std::as_const(crsc)); // don't modify crsc
  check_crsc(std::as_const(crsc));
  check<n>(crsc);
  check_crsc(crsc);

  auto ccsc = fill_ccsc<n>();
  check<n>(std::as_const(ccsc)); // don't modify ccsc
  check<n>(ccsc);
}
