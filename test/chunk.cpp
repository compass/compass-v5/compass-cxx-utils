#include <catch2/catch_test_macros.hpp>

#include <algorithm>
#include <iostream>
#include <ranges>

// FIXME: should be std::ranges::viewable_range instead of std::ranges::range
static auto print_subrange = [](std::ranges::range auto &&r) {
  std::cout << "[";
  for (int pos{}; auto elem : r)
    std::cout << (pos++ ? " " : "") << elem;
  std::cout << "] ";
};

TEST_CASE("Chunk (no CHECK)") {
  using namespace std::views;

  const auto v = {1, 2, 3, 4, 5, 6};

  for (const unsigned width : iota(1U, 2U + v.size())) {
    auto const chunks = v | chunk(width);
    std::cout << "chunk(" << width << "): ";
    std::ranges::for_each(chunks, print_subrange);
    std::cout << '\n';
  }
}
