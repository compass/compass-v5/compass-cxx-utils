#include <catch2/catch_test_macros.hpp>

#include "compass_cxx_utils/combinations.h"
#include "compass_cxx_utils/pretty_print.h"

TEST_CASE("Combinations (no CHECK)") {
  using namespace compass::utils;
  Pretty_printer print;
  std::cout << "Combinations of 1 amongst 2:" << std::endl;
  for (auto &&a : combinations<1, 2>{})
    print(a);
  std::cout << "Combinations of 0 amongst 4:" << std::endl;
  for (auto &&a : combinations<0, 4>{})
    print(a);
  std::cout << "Combinations of 2 amongst 4:" << std::endl;
  for (auto &&a : combinations<2, 4>{})
    print(a);
  std::cout << "Combinations of 4 amongst 4:" << std::endl;
  for (auto &&a : combinations<4, 4>{}) {
    print(a);
  }
}
