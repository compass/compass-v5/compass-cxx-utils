#include <catch2/catch_test_macros.hpp>

#include <iostream>

#include <compass_cxx_utils/csr.h>

// 0X0X0
// X000X
// 0X0X0
void check(const auto &A) {

  using std::views::iota;

  CHECK(A.nb_rows == 3);
  CHECK(A.nb_cols == 5);
  CHECK(A.number_of_nonzeros() == 6);

  CHECK(A.is_zero(0, 0));
  CHECK(A.is_nonzero(0, 1));
  CHECK(A.is_zero(0, 2));
  CHECK(A.is_nonzero(0, 3));
  CHECK(A.is_zero(0, 4));

  CHECK(A.is_nonzero(1, 0));
  CHECK(A.is_zero(1, 1));
  CHECK(A.is_zero(1, 2));
  CHECK(A.is_zero(1, 3));
  CHECK(A.is_nonzero(1, 4));

  CHECK(A.is_zero(2, 0));
  CHECK(A.is_nonzero(2, 1));
  CHECK(A.is_zero(2, 2));
  CHECK(A.is_nonzero(2, 3));
  CHECK(A.is_zero(2, 4));
}

template <typename index_type = int, int nnzpr = -1>
void build_and_check(std::ranges::range auto &&nz) {
  using CSR = compass::utils::CSR<index_type, nnzpr>;
  CSR M{{3, 5}, std::forward<decltype(nz)>(nz)};
  check(M);
}

TEST_CASE("CSR") {

  using CSR = compass::utils::CSR<int>;
  using ivec = std::vector<int>;

  CSR A{{1, 1}, std::vector<ivec>{{}}};
  CHECK(A.is_zero(0, 0));
  CSR B{{1, 1}, std::vector<ivec>{{0}}};
  CHECK(B.is_nonzero(0, 0));

  build_and_check(std::vector<ivec>{{1, 3}, {0, 4}, {1, 3}});
  build_and_check(std::vector<ivec>{{3, 1}, {4, 0}, {1, 3}});
  build_and_check<std::size_t>(std::vector<ivec>{{1, 3}, {0, 4}, {1, 3}});
  build_and_check<int, 2>(std::vector<ivec>{{1, 3}, {0, 4}, {1, 3}});
  build_and_check<int, 2>(std::vector<ivec>{{3, 1}, {4, 0}, {1, 3}});
  build_and_check<int, 2>(ivec{1, 3, 0, 4, 1, 3});
  build_and_check<int, 2>(ivec{3, 1, 4, 0, 3, 1});
}
