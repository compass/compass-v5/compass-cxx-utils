#include <catch2/catch_test_macros.hpp>
#include <concepts>

#include <compass_cxx_utils/enum_set.h>

using namespace compass::utils;

enum struct Phase : std::size_t;
template <Phase... phase> using Phase_set = Enum_set<Phase, phase...>;

enum struct Phase : std::size_t { gas, liquid };

using all_phases = Phase_set<Phase::gas, Phase::liquid>;

TEST_CASE("enumerate set index") {
  STATIC_REQUIRE(Enum_sequence<all_phases>);
  CHECK(index<all_phases>(Phase::gas) == 0);
  CHECK(index<all_phases>(Phase::liquid) == 1);
}
