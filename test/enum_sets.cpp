#include <catch2/catch_test_macros.hpp>
#include <cmath>
#include <string>

#include "compass_cxx_utils/enum_functors.h"
#include "compass_cxx_utils/enum_set.h"
#include "compass_cxx_utils/enum_tuple.h"
#include "compass_cxx_utils/pretty_print.h"
#include "compass_cxx_utils/tricks/unused-variable.h"

#include <iostream>

namespace cu = compass::utils;

enum struct Phase : std::size_t { gas, liquid };

template <Phase... phase> using Phase_set = cu::Enum_set<Phase, phase...>;

using all_phases = Phase_set<Phase::gas, Phase::liquid>;
constexpr auto nb_phases = all_phases::size;

template <typename... Fs>
  requires(sizeof...(Fs) == nb_phases)
auto phase_functors(Fs &&...fs) {
  return cu::Enum_functors<all_phases, std::decay_t<Fs>...>{
      std::forward_as_tuple(std::forward<Fs>(fs)...)};
}

struct Rho {
  double param; // a functor parameter
  auto operator()(const double &x) const { return param * x; }
};

struct Zero {
  constexpr std::size_t operator()(auto &&...) const { return 0; }
};

struct Zero_as_string {
  constexpr std::string operator()(auto &&...) const { return "zero"; }
};

struct Count_args {
  constexpr std::size_t operator()(auto &&...x) const { return sizeof...(x); }
};

TEST_CASE("enumerate sets") {
  using single_phase = Phase_set<Phase::gas>;
  using two_phases = Phase_set<Phase::gas, Phase::liquid>;

  auto rho1 = Rho{.param = 1};
  auto rho2 = Rho{.param = 2};
  auto F = phase_functors(rho1, rho2);

  auto x = F.sum<single_phase>(1);
  std::cout << x << std::endl;
  x = F.sum<two_phases>(1);
  std::cout << x << std::endl;
  // x = F[Phase::gas](1);       // will look for Phase::gas
  x = F.get<Phase::gas>()(1); // compile-time deduction
  std::cout << x << std::endl;

  auto G = phase_functors(Zero{}, Count_args{});

  CHECK(G.sum() == 0);
  CHECK(G.sum(1) == 1);
  CHECK(G.sum(1, 1) == 2);

  CHECK(G.apply<Phase::gas>() == 0);
  CHECK(G.apply<Phase::gas>(2) == 0);
  CHECK(G.apply<Phase::gas>(2, 3) == 0);
  CHECK(G.apply<Phase::liquid>() == 0);
  CHECK(G.apply<Phase::liquid>(2) == 1);
  CHECK(G.apply<Phase::liquid>(2, 3) == 2);
  CHECK(G.apply(Phase::liquid) == 0);
  CHECK(G.apply(Phase::liquid, 2) == 1);
  CHECK(G.apply(Phase::liquid, 2, 3) == 2);

  // _apply should not be used directly
  CHECK(G._apply(G.as_index(Phase::liquid), 2, 3) == 2);

  // evaluate as an array when the return type are the same
  auto a = G(1, 2);
  CHECK(a[0] == 0); // Zero{}(1,2);
  CHECK(a[1] == 2); // Count_args{}(1,2);

  auto H = phase_functors(Zero_as_string{}, Count_args{});
  // evaluate as a tuple when the return type are not the same
  auto t = H(1, 2);
  CHECK(std::get<0>(t) == Zero_as_string{}());
  CHECK(std::get<1>(t) == 2); // Count_args{}(1,2);

  // you can explicitely require evaluation as an array
  auto a2 = G.evaluate_as_array(1, 2);
  cu::pretty_print(a2);
  // auto a3 = H.evaluate_as_array(1,2); // this will fail with a nice
  // compilation error message
  auto t2 = G.evaluate_as_tuple(1, 2);
  cu::pretty_print(t2);
  CHECK(std::get<1>(t2) == 2); // Count_args{}(1,2);

  auto S = cu::composition<cu::binops::add>(Zero{}, Count_args{});
  CHECK(S() == 0);
  CHECK(S(1) == 1);
  CHECK(S(1, 2) == 2);

  // can be defined but compilation would fail on evaluation
  auto GpH = cu::composition<cu::binops::add>(G, H);
  UNUSED_VARIABLE(GpH); // FIXME: check something !

  auto GpG = G + G;
  a = GpG(1, 2);
  CHECK(a[0] == 0); // 0 + 0
  CHECK(a[1] == 4); // 2 + 2;
  auto GmG = G - G;
  a = GmG(1, 2);
  CHECK(a[0] == 0); // 0 - 0
  CHECK(a[1] == 0); // 2 - 2;
  auto GxG = G * G;
  a = GxG(1, 2, 3);
  CHECK(a[0] == 0); // 0 * 0
  CHECK(a[1] == 9); // 3 * 3;

  auto G2 = phase_functors(rho2, Count_args{});
  auto GdG = G2 / G2;
  auto b = GdG(1);
  CHECK(std::get<0>(b) == 1);
  CHECK(std::get<1>(b) == 1);
}
