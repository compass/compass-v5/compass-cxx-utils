#include <catch2/catch_test_macros.hpp>
#include <iostream>
#include <ranges>

TEST_CASE("enumerate") {
  using namespace std::views;

  double a[] = {0, 0.1, 0.2, 0.3, 0.4};

  // basic usage
  for (auto &&[i, x] : enumerate(a)) {
    CHECK(abs(x - 0.1 * i) < 1e-14);
    std::cout << i << ": " << x << std::endl;
  }

  // selecting the counter type
  for (auto &&[c, x] : enumerate(a)) {
    std::cout << (char)('a' + c) << ": " << x << std::endl;
  }
}
