#include <catch2/catch_test_macros.hpp>
#include <iostream>

#include "compass_cxx_utils/ipow.h"

TEST_CASE("ipow") {
  using compass::utils::ipow;
  for (int i = 0; i < 10; ++i) {
    CHECK(ipow(2, i) == (1 << i));
    std::cout << ipow(2, i) << std::endl;
  }
}
