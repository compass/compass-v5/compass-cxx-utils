#include <array>
#include <catch2/catch_test_macros.hpp>
#include <iostream>

#include <compass_cxx_utils/self-managed.h>

using namespace compass::utils;

struct ISet {

  struct data : Self_managed_object<data> {
    std::size_t size;
    handle base;
  };

  data::handle self;

  ISet() {
    CHECK(!self);
    self = data::make();
    CHECK(self->use_count() == 1);
  }
};

TEST_CASE("iset prototype") { ISet I; }
