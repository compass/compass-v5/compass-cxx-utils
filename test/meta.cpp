#include "compass_cxx_utils/meta.h"
#include <catch2/catch_test_macros.hpp>
#include <iostream>

template <std::size_t... nk> using IS = std::index_sequence<nk...>;

using namespace compass::meta::index_sequence;
using It = Iterator<IS<0, 0>, IS<3, 3>>;

template <typename IS, typename OS> inline OS &dump(OS &os) {
  if constexpr (len_v<IS> != 0) {
    if constexpr (len_v<IS> == 1) {
      os << last_v<IS>;
    } else {
      dump<drop_t<1, IS>>(os) << " " << last_v<IS>;
    }
  }
  return os;
}

TEST_CASE("meta") {
  STATIC_REQUIRE(std::is_same_v<append_t<IS<1, 2>, 3>, IS<1, 2, 3>>);
  STATIC_REQUIRE(std::is_same_v<prepend_t<0, IS<1, 2>>, IS<0, 1, 2>>);
  STATIC_REQUIRE(std::is_same_v<keep_t<0, IS<1, 2, 3>>, IS<>>);
  STATIC_REQUIRE(std::is_same_v<keep_t<1, IS<1, 2, 3>>, IS<1>>);
  STATIC_REQUIRE(std::is_same_v<keep_t<2, IS<1, 2, 3>>, IS<1, 2>>);
  STATIC_REQUIRE(std::is_same_v<keep_t<42, IS<1, 2, 3>>, IS<1, 2, 3>>);
  STATIC_REQUIRE(std::is_same_v<drop_t<0, IS<1, 2, 3>>, IS<1, 2, 3>>);
  STATIC_REQUIRE(std::is_same_v<drop_t<1, IS<1, 2, 3>>, IS<1, 2>>);
  STATIC_REQUIRE(std::is_same_v<drop_t<2, IS<1, 2, 3>>, IS<1>>);
  STATIC_REQUIRE(std::is_same_v<drop_t<5, IS<1, 2, 3>>, IS<>>);
  STATIC_REQUIRE(
      std::is_same_v<fill_t<2, 42, IS<1, 2, 3>>, IS<1, 2, 3, 42, 42>>);
  STATIC_REQUIRE(compass::meta::index_sequence::index<2, IS<1, 2, 3>>::value ==
                 3);
  STATIC_REQUIRE(index_v<2, IS<1, 2, 3>> == 3);
  STATIC_REQUIRE(len_v<IS<1, 2, 3>> == 3);
  STATIC_REQUIRE(last_v<IS<1, 2, 3>> == 3);
  STATIC_REQUIRE(last_v<IS<1, 2>> == 2);
  STATIC_REQUIRE(last_v<IS<42>> == 42);
  STATIC_REQUIRE(
      std::is_same_v<next_t<IS<1, 2>, IS<0, 0>, IS<3, 3>>, IS<2, 0>>);
  STATIC_REQUIRE(
      std::is_same_v<next_t<IS<1, 2>, IS<0, 0>, IS<3, 3>>, IS<2, 0>>);
  STATIC_REQUIRE(!stop_iteration_v<IS<1, 2>, IS<0, 0>, IS<3, 3>>);
  STATIC_REQUIRE(stop_iteration_v<IS<2, 2>, IS<0, 0>, IS<3, 3>>);
  STATIC_REQUIRE(std::is_same_v<It::next<IS<1, 2>>, IS<2, 0>>);
  STATIC_REQUIRE(!It::stop<IS<1, 2>>);
  STATIC_REQUIRE(It::stop<IS<2, 2>>);
  STATIC_REQUIRE(It::has_next<IS<1, 2>>);
  STATIC_REQUIRE(!It::has_next<IS<2, 2>>);
  dump<next_t<IS<1, 2>, IS<0, 0>, IS<3, 3>>>(std::cout) << std::endl;
  dump<next_t<IS<1, 2>, IS<0, 0>, IS<3, 3>>>(std::cout) << std::endl;
  dump<It::next<IS<1, 2>>>(std::cout) << std::endl;
}
