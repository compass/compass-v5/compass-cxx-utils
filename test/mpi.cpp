#include <algorithm>
#include <cassert>
#include <iostream>
#include <ranges>
#include <vector>

#include <compass_cxx_utils/mpi.h>
// #include "compass_cxx_utils/enumerate.h"

namespace mpi = compass::mpi;

struct Foo {
  char c;
  int i;
  double x;
};

template <typename T>
void dump(const std::vector<T> &v, const mpi::Communicator &comm) {
  if (v.empty()) {
    std::cout << "Vector is empty on proc " << comm.rank() << "/" << comm.size()
              << std::endl;
  } else {
    std::cout << "processor " << comm.rank() << "/" << comm.size() << " with "
              << v.size() << " pieces of data: ";
    for (auto &&x : v)
      std::cout << x << " ";
    std::cout << '\n';
  }
}

void send_arrays_point_to_point(const mpi::Communicator &comm) {

  const int sender = 0;
  const int receiver = 1;

  int a[2] = {-1, -1};
  if (comm.rank() == sender) {
    a[0] = 1;
    a[1] = 2;
  }
  comm.exchange(sender, receiver, a);

  std::cout << "Array on rank " << comm.rank() << ": " << a[0] << " " << a[1]
            << std::endl;
}

void send_point_to_point(const mpi::Communicator &comm) {

  const int sender = 0;
  const int receiver = 1;

  int i = 0;
  if (comm.rank() == sender) {
    i = 42;
  }
  comm.exchange(sender, receiver, i);

  std::vector<double> v;
  if (comm.rank() == sender) {
    // prepare what will be sent
    // could typically be done inside a function
    for (auto &&x : {1., 2., 3., 4., 5.}) {
      v.emplace_back(x);
    }
  }
  comm.exchange(0, 1, v);
  dump(v, comm);

  std::vector<Foo> vf;
  if (comm.rank() == sender) {
    vf.emplace_back('a', 42, 3.14);
  }
  comm.exchange(sender, receiver, vf);
  if (comm.rank() == sender || comm.rank() == receiver) {
    assert(vf.size() == 1);
    assert(vf.back().c == 'a');
    assert(vf.back().i == 42);
    assert(vf.back().x == 3.14);
  } else {
    assert(vf.empty());
  }

  if (comm.rank() == sender) {
    for (i = 0; i < 4; ++i)
      vf.emplace_back('a', 42, 3.14);
  } else {
    vf.clear();
  }
  comm.exchange(sender, receiver, vf);
  if (comm.rank() == sender || comm.rank() == receiver) {
    assert(vf.size() == 5);
    for (auto &&F : vf) {
      assert(F.c == 'a');
      assert(F.i == 42);
      assert(F.x == 3.14);
    }
  } else {
    assert(vf.empty());
  }
}

void distribute(const mpi::Communicator &comm) {

  const int sender = 0;
  constexpr int part_size = 5;

  using vector = std::vector<double>;
  vector v;
  std::vector<vector> parts;
  if (comm.rank() == sender) {
    // prepare what will be sent
    // could typically be done inside a function
    const auto n = comm.size();
    parts.resize(n);
    for (int i = 0; i < part_size * n; ++i) {
      parts[i % n].emplace_back(i);
    }
    v = comm.scatter(parts);
  } else {
    comm.receive_part(sender, v);
  }
  dump(v, comm);
}

auto _global_indices(const mpi::Communicator &comm, const std::size_t part_size,
                     const std::size_t nb_ghosts_left,
                     const std::size_t nb_ghosts_right) {
  assert(nb_ghosts_left + nb_ghosts_right < part_size);
  const auto rank = comm.rank();
  std::size_t start = rank * part_size;
  std::size_t end = start + part_size;
  if (rank > 0)
    start -= nb_ghosts_left;
  if (comm.rank() + 1 < comm.size())
    end += nb_ghosts_right;
  std::vector<std::size_t> result;
  result.reserve(end - start);
  for (auto &&i : std::views::iota(start, end))
    result.push_back(i);
  return result;
}

template <typename Initialization, typename Propagation, typename Check>
void _propagation(const mpi::Communicator &comm, Initialization &&init,
                  Propagation &&propagate, Check &&check,
                  const std::size_t part_size, const std::size_t nb_ghosts_left,
                  const std::size_t nb_ghosts_right) {

  using State = int;
  using Synchronizer = mpi::Synchronizer;
  assert(nb_ghosts_left + nb_ghosts_right < part_size);
  std::size_t nbcells = part_size;
  std::vector<Synchronizer::info> sent;
  std::vector<Synchronizer::info> received;
  std::vector<std::size_t> owns;
  std::vector<std::size_t> ghosts;
  if (comm.rank() > 0)
    nbcells += nb_ghosts_left;
  if (comm.rank() + 1 < comm.size())
    nbcells += nb_ghosts_right;
  auto mesh = std::make_unique<State[]>(nbcells);
  {
    // on the left
    if (comm.rank() > 0) {
      received.emplace_back(comm.rank() - 1, nb_ghosts_left);
      for (std::size_t i = 0; i < nb_ghosts_left; ++i)
        ghosts.push_back(i);
      sent.emplace_back(comm.rank() - 1, nb_ghosts_right);
      for (std::size_t i = 0; i < nb_ghosts_right; ++i)
        owns.push_back(i + nb_ghosts_left);
    }
  }
  {
    // on the right
    if (comm.rank() + 1 < comm.size()) {
      // offset to the first cell sent on the right
      std::size_t offset = part_size;
      // inside we have: offset = nb_ghosts_left + part_size - nb_ghosts_left
      if (comm.rank() == 0)
        offset -= nb_ghosts_left;
      sent.emplace_back(comm.rank() + 1, nb_ghosts_left);
      for (std::size_t i = 0; i < nb_ghosts_left; ++i)
        owns.push_back(offset + i);
      offset += nb_ghosts_left; // offset to the first ghost on the right
      received.emplace_back(comm.rank() + 1, nb_ghosts_right);
      for (std::size_t i = 0; i < nb_ghosts_right; ++i)
        ghosts.push_back(offset + i);
    }
  }
  Synchronizer sync{comm, sent, received};
  std::vector<State> buffer;
  buffer.resize(sync.buffer_size());
  // initial value
  auto states = std::span{mesh.get(), nbcells};
  init(states);
  const auto global_indices =
      _global_indices(comm, part_size, nb_ghosts_left, nb_ghosts_right);
  for (std::size_t t = 0; t != comm.size() * part_size; ++t) {
    check(states, t, global_indices);
    propagate(states);
    // copy non-ghost to buffer
    auto p = buffer.begin();
    for (auto &&i : owns) {
      *p = states[i];
      ++p;
    }
    // synwhronization step (must be called by all procs)
    sync.synchronize(buffer);
    // copy buffer to ghosts
    for (auto &&i : ghosts) {
      states[i] = *p;
      ++p;
    }
    assert(p == buffer.end());
  }
}

// #include <sstream>

void propagation(const mpi::Communicator &comm, const std::size_t part_size) {
  // 1D mesh with one layer of ghost cell
  // part_size own cells per mesh
  // if propagation is left to right we only need ghost cell on the left
  auto init_left = [&comm](auto &&states) {
    for (auto &&x : states)
      x = 0;
    if (comm.rank() == 0)
      states[0] = 1;
  };
  auto left_to_right = [](auto &&states) {
    for (std::size_t k = 1; k < states.size(); ++k) {
      // copy right to left not to fill the state vector in a single call
      const auto rev = states.size() - k;
      states[rev] = states[rev - 1];
    }
  };
  auto check_left_to_right = [&comm](auto &&states, const std::size_t t,
                                     auto &&global_indices) {
    //// output states for debug - don't forget to include <sstream>
    // std::stringstream ss;
    // ss << "time " << t << " on rank " << comm.rank() << ":";
    // for (auto &&x : states)
    //   ss << " " << x;
    // ss << std::ends;
    // std::cout << ss.str() << std::endl;
    // comm.barrier();
    std::size_t l = 0;
    for (auto &&g : global_indices) {
      assert((t < g && states[l] == 0) or (t >= g && states[l] == 1));
      ++l;
    }
  };
  _propagation(comm, init_left, left_to_right, check_left_to_right, part_size,
               1, 0);
  _propagation(comm, init_left, left_to_right, check_left_to_right, part_size,
               1, 1);
  _propagation(comm, init_left, left_to_right, check_left_to_right, part_size,
               2, 1);
  _propagation(comm, init_left, left_to_right, check_left_to_right, part_size,
               1, 2);
  _propagation(comm, init_left, left_to_right, check_left_to_right, part_size,
               2, 2);
  auto init_right = [&comm](auto &&states) {
    for (auto &&x : states)
      x = 0;
    if (comm.rank() + 1 == comm.size())
      states.back() = 1;
  };
  auto right_to_left = [](auto &&states) {
    for (std::size_t k = 0; k + 1 < states.size(); ++k) {
      states[k] = states[k + 1];
    }
  };
  auto check_right_to_left = [&comm, part_size](auto &&states,
                                                const std::size_t t,
                                                auto &&global_indices) {
    //// output states for debug - don't forget to include <sstream>
    // std::stringstream ss;
    // ss << "time " << t << " on rank " << comm.rank() << ":";
    // for (auto &&x : states)
    //   ss << " " << x;
    // ss << std::ends;
    // std::cout << ss.str() << std::endl;
    // comm.barrier();
    std::size_t l = 0;
    for (auto &&g : global_indices) {
      auto rev = comm.size() * part_size - (g + 1);
      assert((t < rev && states[l] == 0) or (t >= rev && states[l] == 1));
      ++l;
    }
  };
  _propagation(comm, init_right, right_to_left, check_right_to_left, part_size,
               0, 1);
  _propagation(comm, init_right, right_to_left, check_right_to_left, part_size,
               1, 1);
  _propagation(comm, init_right, right_to_left, check_right_to_left, part_size,
               2, 1);
  _propagation(comm, init_right, right_to_left, check_right_to_left, part_size,
               1, 2);
  _propagation(comm, init_right, right_to_left, check_right_to_left, part_size,
               2, 2);
}

int main(int argc, char *argv[]) {

  MPI_Init(&argc, &argv);

  auto comm = mpi::Communicator();

  send_point_to_point(comm);

  comm.barrier();

  send_arrays_point_to_point(comm);

  comm.barrier();

  distribute(comm);

  comm.barrier();

  propagation(comm, 5);
  propagation(comm, 10);

  MPI_Finalize();
}
