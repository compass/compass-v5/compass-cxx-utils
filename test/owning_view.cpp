#include <catch2/catch_test_macros.hpp>
#include <iostream>
#include <ranges>
#include <string>

TEST_CASE("owning view") {
  using namespace std::literals;
  using namespace std::ranges;
  using namespace std::views;

  owning_view ov{"cosmos"s}; // the deduced type of R is std::string;
                             // `ov` is the only owner of this string
  CHECK((ov.empty() == false and ov.size() == 6 and
         ov.size() == ov.base().size() and ov.front() == 'c' and
         ov.front() == *ov.begin() and ov.back() == 's' and
         ov.back() == *(ov.end() - 1) and ov.data() == ov.base()));

  std::cout << "sizeof(ov): " << sizeof ov
            << '\n' // typically equal to sizeof(R)
            << "range-for: ";
  for (const char ch : ov)
    std::cout << ch;
  std::cout << '\n';

  owning_view<std::string> ov2;
  CHECK(ov2.empty());
  // ov2 = ov; // compile-time error: copy assignment operator is deleted
  ov2 = std::move(ov);
  CHECK(ov2.size() == 6);

  // drop the first 2
  for (const char ch : std::move(ov2) | drop(2))
    std::cout << ch;
  std::cout << '\n';
}
