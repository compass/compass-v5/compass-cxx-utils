#include <algorithm>
#include <catch2/catch_test_macros.hpp>
#include <iostream>
#include <ranges>
#include <vector>

#include <compass_cxx_utils/pretty_print.h>

TEST_CASE("pretty print") {
  using namespace compass::utils;
  using namespace std::views;
  pretty_print(iota(0, 3));
  pretty_print(iota(0, 20));
  Pretty_printer print;
  print(2.6);
  print(iota(0, 20));
  print("iotas: {", iota(0, 20), "}");
  print(std::make_tuple(1, 3.14, "hello"));
  print("empty list ", iota(0, 0));
  print("empty tuple", std::make_tuple());
}
