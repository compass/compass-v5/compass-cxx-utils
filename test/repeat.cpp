#include <catch2/catch_test_macros.hpp>
#include <iostream>
#include <ranges>
#include <string_view>

TEST_CASE("repeat") {
  using namespace std::literals;
  using namespace std::views;

  // bounded overload
  for (auto s : repeat("C++"sv, 4))
    std::cout << s << ' ';
  std::cout << '\n';
}
