#include <catch2/catch_test_macros.hpp>

#include <array>
#include <iostream>

#include <compass_cxx_utils/self-managed.h>

using namespace compass::utils;

struct Foo : Self_managed_object<Foo> {
  ~Foo() { std::cout << "Foo is destroyed." << std::endl; }
};

struct DataInterface {

  struct Data : Self_managed_object<Data> {
    std::array<double, 2> a;
  };

  Data::handle data;

  DataInterface() : data{} {
    CHECK_FALSE(data);
    data = Data::make();
    CHECK(data->use_count() == 1);
  }
};

struct ISet_data : Self_managed_object<ISet_data> {
  std::size_t size;
  ISet_data(const std::integral auto n) : size{(std::size_t)n} {}
};

struct ISet : ISet_data::handle {
  ISet() = default;
  ISet(std::size_t n) : ISet_data::handle{ISet_data::make(n)} {}
  std::size_t size() const {
    if (!*this)
      return 0;
    return data->size;
  }
};

TEST_CASE("Self-managed objects") {

  auto hf = Foo::make();
  CHECK(hf->use_count() == 1);
  auto hf2 = hf;
  CHECK(hf->use_count() == 2);
  CHECK(hf == hf2);
  auto hf3 = std::move(hf2);
  CHECK(hf);
  CHECK(hf3);
  CHECK(hf == hf3);
  CHECK(hf->use_count() == 2);

  DataInterface data;

  ISet_data::handle h;
  CHECK_FALSE(h);
  CHECK(h.raw() == nullptr);
  h = ISet_data::make(3ul); // FIXME: make it more general
  CHECK(h.raw() != nullptr);
  CHECK(static_cast<const ISet_data *>(h)->size == 3);
  CHECK(static_cast<const ISet_data &>(h).size == 3);
  ISet I{42};
  CHECK(static_cast<const ISet_data &>(I).size == 42);
  CHECK(I.size() == 42);
  I = {};
  CHECK(I.size() == 0);

  CHECK(h->use_count() == 1);
  ISet_data::handle h2 = h.raw();
  CHECK(h->use_count() == 2);
  CHECK(h->use_count() == h2->use_count());
}
