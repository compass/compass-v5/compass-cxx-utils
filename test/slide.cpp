#include <algorithm>
#include <catch2/catch_test_macros.hpp>
#include <iostream>
#include <ranges>

// FIXME: we would like to use std::ranges::viewable_range instead of
// std::ranges::range
static auto print_subrange = [](std::ranges::range auto &&r) {
  std::cout << "[";
  for (int pos{}; auto elem : r)
    std::cout << (pos++ ? " " : "") << elem;
  std::cout << "] ";
};

TEST_CASE("slide") {
  using namespace std::views;

  const auto v = {1, 2, 3, 4, 5, 6};

  for (const unsigned width : iota(1U, 1U + v.size())) {
    auto const windows = v | slide(width);
    std::cout << "All sliding windows of width " << width << ": ";
    std::ranges::for_each(windows, print_subrange);
    std::cout << '\n';
  }

  auto ints = iota(0U, 10U);
  auto sum_by_two = zip(ints, ints | drop(1)) | transform([](auto &&be) {
                      auto &&[i, j] = be;
                      std::cout << i << " + " << j << " = ";
                      return i + j;
                    });
  for (auto &&s : sum_by_two) {
    std::cout << s << std::endl;
  }
  std::cout << std::endl;
  std::cout << "Sum at index 5: " << sum_by_two[5] << std::endl;
}
