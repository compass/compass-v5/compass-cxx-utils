#include <catch2/catch_test_macros.hpp>
#include <iostream>
#include <ranges>

#include <compass_cxx_utils/pretty_print.h>

using namespace std::literals;

TEST_CASE("stride") {
  using namespace std::views;
  compass::utils::Pretty_printer print;

  print(iota(1, 13) | stride(3));
  print(iota(1, 13) | stride(3) | reverse);
  print(iota(1, 13) | reverse | stride(3));

  print("0x0!133713337*x//42/A$"sv | stride(0B11) |
            transform([](char O) -> char { return 0100 + O; }),
        "");
}
