#include <catch2/catch_test_macros.hpp>
#include <compass_cxx_utils/pretty_print.h>
#include <compass_cxx_utils/tensor.h>

#include <algorithm>
#include <iostream>
#include <utility>

#include <Eigen/Dense>

using compass::utils::Tensor;

template <typename T1, typename T2, int nr, int nc, std::size_t n0,
          std::size_t n1>
bool operator==(const Eigen::Array<T1, nr, nc> &m,
                const Tensor<T2, n0, n1> &t) {
  if constexpr (!std::cmp_equal(n0, nr))
    return false;
  if constexpr (!std::cmp_equal(n1, nc))
    return false;
  CHECK(std::cmp_equal(m.size(), t.size()));
  auto e = t.data() + t.size();
  auto [m1, m2] = std::mismatch(t.data(), e, m.data());
  return m1 == e && m2 == m.data() + m.size();
}

template <typename T1, typename T2, int nr, int nc, std::size_t n0,
          std::size_t n1>
bool operator==(const Tensor<T1, n0, n1> &t,
                const Eigen::Array<T2, nr, nc> &m) {
  return m == t;
}

template <typename T, int nr, int nc>
  requires(nr > 0 and nc > 0)
auto tensor(const Eigen::Array<T, nr, nc> &m) {
  Tensor<T, (std::size_t)nr, (std::size_t)nc> result;
  std::copy(m.data(), m.data() + m.size(), result.data());
  return result;
}

template <typename T> void dump(const T &t) {
  for (auto &&x : std::span{t.data(), t.data() + t.size()})
    std::cout << x << " ";
  std::cout << std::endl;
}

void construction() {

  {

    Tensor<int, 2> t{{1, 2}};

    CHECK(t(0) == 1);
    CHECK(t(1) == 2);
  }

  {

    Tensor<int, 2> t;

    t = {1, 2};
    CHECK(t(0) == 1);
    CHECK(t(1) == 2);

    t = {1., 2.};
    CHECK(t(0) == 1);
    CHECK(t(1) == 2);
  }

  {

    Tensor<double, 2, 3> t{{{3.14, 42., 0.}, {0., 3.14, 42.}}};
    CHECK(t(0, 0) == 3.14);
    CHECK(t(0, 1) == 42.);
    CHECK(t(0, 2) == 0.);
    CHECK(t(1, 0) == 0.);
    CHECK(t(1, 1) == 3.14);
    CHECK(t(1, 2) == 42.);
  }

  {

    Tensor<double, 2, 3> t;

    t = {{1, 2, 3}, {1, 2, 3}};
    CHECK(t(0, 0) == 1.);
    CHECK(t(0, 1) == 2.);
    CHECK(t(0, 2) == 3.);
    CHECK(t(1, 0) == 1.);
    CHECK(t(1, 1) == 2.);
    CHECK(t(1, 2) == 3.);

    t = {{3.14, 42., 0.}, {0., 3.14, 42.}};
    CHECK(t(0, 0) == 3.14);
    CHECK(t(0, 1) == 42.);
    CHECK(t(0, 2) == 0.);
    CHECK(t(1, 0) == 0.);
    CHECK(t(1, 1) == 3.14);
    CHECK(t(1, 2) == 42.);
  }

  {

    // using deduction guides
    auto t = Tensor{{1, 2, 3}};
    STATIC_REQUIRE(std::is_same_v<typename decltype(t)::value_type, int>);
    STATIC_REQUIRE(t.ndim() == 1);
    STATIC_REQUIRE(t.size() == 3);
    STATIC_REQUIRE(t.shape()[0] == 3);
    CHECK(t(0) == 1);
    CHECK(t(1) == 2);
    CHECK(t(2) == 3);
  }

  {

    // using deduction guides

    auto t = Tensor{{{3.14, 42., 0.}, {0., 3.14, 42.}}};
    STATIC_REQUIRE(std::is_same_v<typename decltype(t)::value_type, double>);
    STATIC_REQUIRE(t.ndim() == 2);
    STATIC_REQUIRE(t.size() == 6);
    STATIC_REQUIRE(t.shape()[0] == 2);
    STATIC_REQUIRE(t.shape()[1] == 3);
    CHECK(t(0, 0) == 3.14);
    CHECK(t(0, 1) == 42.);
    CHECK(t(0, 2) == 0.);
    CHECK(t(1, 0) == 0.);
    CHECK(t(1, 1) == 3.14);
    CHECK(t(1, 2) == 42.);
  }
}

template <int nr, int nc> void various_operations() {

  using Array = Eigen::Array<double, nr, nc>;

  Array a = Array::Random(); // don't use auto with Eigen types because of lazy
                             // evaluation
  auto t = tensor(a);
  CHECK((a == t));

  a = 0;
  t = 0;
  CHECK((a == t));
  a = 3.14;
  t = 3.14;
  CHECK((a == t));

  a = Array::Random();
  t = tensor(a);
  CHECK((a == t));

  a += a;
  t += t;
  CHECK((a == t));

  a += 42;
  t += 42;
  CHECK((a == t));

  a -= 2.3;
  t -= 2.3;
  CHECK((a == t));

  a *= 3.14;
  t *= 3.14;
  CHECK((a == t));

  a /= 2;
  t /= 2;
  CHECK((a == t));

  Array a1 = Array::Random();
  Array a2 = Array::Random();
  auto t1 = tensor(a1);
  auto t2 = tensor(a2);
  CHECK((a1 == t1));
  CHECK((a2 == t2));

  a1 += a2;
  t1 += t2;
  CHECK((a1 == t1));

  a1 -= a2;
  t1 -= t2;
  CHECK((a1 == t1));

  a1 *= a2;
  t1 *= t2;
  CHECK((a1 == t1));

  a1 /= a2;
  t1 /= t2;
  CHECK((a1 == t1));

  a = a1 + a2; // assign to handle the right Eigen type
  CHECK((a == t1 + t2));

  a = a2 - a1; // assign to handle the right Eigen type
  CHECK((a == t2 - t1));

  a = a1 * a2; // assign to handle the right Eigen type
  CHECK((a == t1 * t2));

  a = a1 / a2; // assign to handle the right Eigen type
  CHECK((a == t1 / t2));

  a = a1 + 42;
  CHECK((a == t1 + 42));
  CHECK((a == 42 + t1));

  a = a2 - 3.14;
  CHECK((a == t2 - 3.14));
  CHECK((a == -1 * (3.14 - t2)));

  a = a1 * 1.234;
  CHECK((a == t1 * 1.234));
  CHECK((a == 1.234 * t1));

  a = a2 / 3.14;
  CHECK((a == t2 / 3.14));

  a = 3.14 / a1;
  CHECK((a == 3.14 / t1));

  a = -a1;
  CHECK((a == -t1));
}

void miscellaneous() {

  compass::utils::Pretty_printer print;

  constexpr int nr = 3;
  constexpr int nc = 4;
  Tensor<int, nr, nc> T;

  // set the values
  for (int i = 0; i < nr; ++i) {
    for (int j = 0; j < nc; ++j) {
      T(i, j) = i + j;
    }
  }
  auto p = T.data();
  for (int i = 0; i < nr; ++i) {
    for (int j = 0; j < nc; ++j) {
      CHECK(*(p + i * nc + j) == (i + j));
    }
  }

  // the tensor shape
  // print("shape: ", T.shape());
  auto [i, j] = T.shape();
  CHECK(i == nr);
  CHECK(j == nc);

  // all tensor values
  dump(T);
}

template <typename Scalar> auto det(const Tensor<Scalar, 2, 2> &T) {
  return T(0, 0) * T(1, 1) - T(1, 0) * T(0, 1);
}

template <typename Scalar> auto inv(const Tensor<Scalar, 2, 2> &T) {
  const auto d = det(T);
  CHECK(d != 0);
  return decltype(T){{{T(1, 1), -T(0, 1)}, {T(1, 0), T(0, 0)}}} / d;
}

void inverse() {

  const Tensor<double, 2, 2> Id{{{1, 0}, {0, 1}}};
  auto I = inv(Id);
  auto diff = I - Id;
  for (auto &&x : diff) {
    CHECK(fabs(x) < 1e-10);
  }
}

void subtensors() {

  Tensor<int, 2, 3> T{{{0, 1, 2}, {3, 4, 5}}};

  using row_t = compass::utils::detail::drop_t<1, decltype(T)>;

  STATIC_REQUIRE(row_t::ndim() == 1);
  STATIC_REQUIRE(row_t::size() == 3);

  auto &row0 = T(0);

  STATIC_REQUIRE(std::is_same_v<std::decay_t<decltype(row0)>, Tensor<int, 3>>);

  row0(1) = 42;
  CHECK(T(0, 1) == 42);

  Tensor<int, 3> a{{10, 11, 12}};
  row0 = a;

  CHECK(std::ranges::equal(T, Tensor<int, 2, 3>{{{10, 11, 12}, {3, 4, 5}}}));

  auto &row1 = std::as_const(T)(1);

  STATIC_REQUIRE(std::is_same_v<std::decay_t<decltype(row1)>, Tensor<int, 3>>);

  CHECK(std::ranges::equal(row1, Tensor<int, 3>{{3, 4, 5}}));

  CHECK(row1(2) == 5);

  // row1(0) = 0; discards const qualifier

  Tensor<int, 2, 2, 2> M;

  auto &B = M(0);
  STATIC_REQUIRE(std::is_same_v<std::decay_t<decltype(B)>, Tensor<int, 2, 2>>);
  auto &C = M(0, 0);
  STATIC_REQUIRE(std::is_same_v<std::decay_t<decltype(C)>, Tensor<int, 2>>);

  B = Tensor<int, 2, 2>{{{0, 1}, {2, 3}}};
  M(1) = Tensor<int, 2, 2>{{{4, 5}, {6, 7}}};

  CHECK(std::ranges::equal(M, std::views::iota(0, 8)));
}

void assignments() {

  Tensor<int, 2, 3> T;

  T(0) = std::array<double, 3>{0, 1, 2};
  T(1) = std::array<int, 3>{3, 4, 5};

  CHECK(std::ranges::equal(T, std::views::iota(0, 6)));

  T = std::array<int, 6>{6, 7, 8, 9, 10, 11};

  CHECK(std::ranges::equal(T, std::views::iota(6, 12)));

  // range assignments

  Tensor<int, 2, 3> M;

  std::vector<int> v{0, 1, 2, 3, 4, 5};

  std::ranges::copy(v, M.begin());
  CHECK(std::ranges::equal(M, std::views::iota(0, 6)));

  std::ranges::copy(std::views::iota(10, 16), M.begin());
  CHECK(std::ranges::equal(M, std::views::iota(10, 16)));

  std::ranges::copy(std::views::iota(0, 3), M(1).begin());
  CHECK(std::ranges::equal(M, Tensor<int, 2, 3>{{{10, 11, 12}, {0, 1, 2}}}));
}

TEST_CASE("tensor") {
  construction();

  miscellaneous();

  various_operations<1, 1>();
  various_operations<1, 2>();
  various_operations<1, 3>();
  various_operations<2, 1>();
  various_operations<2, 2>();
  various_operations<2, 3>();
  various_operations<3, 1>();
  various_operations<3, 2>();
  various_operations<3, 3>();

  inverse();

  subtensors();

  assignments();
}
