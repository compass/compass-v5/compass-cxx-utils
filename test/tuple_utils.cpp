#include <array>
#include <catch2/catch_test_macros.hpp>

#include <compass_cxx_utils/tuple_utils.h>

TEST_CASE("tuple utils") {
  std::array<int, 2> a{42, 123};
  auto t = compass::utils::as_tuple(a);
  CHECK(std::get<0>(t) == a[0]);
  CHECK(std::get<1>(t) == a[1]);
}
