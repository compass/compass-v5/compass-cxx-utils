#include <catch2/catch_test_macros.hpp>
#include <iostream>

#include <compass_cxx_utils/vsarray.h>

TEST_CASE("vsarray") {
  using namespace compass::utils;
  using T = std::uint8_t;
  constexpr std::array<T, 3> a3{1, 2, 3};
  static constexpr vsarray<T, 4> vsdefault;
  constexpr vsarray<T, 4> array[3] = {{1, 2}, a3, {}};
  for (auto &&a : array) {
    for (auto &&x : a)
      std::cout << (int)x << " ";
    std::cout << std::endl;
  }
  for (auto &&x : array[1])
    std::cout << (int)x << " ";
  std::cout << std::endl;
}
