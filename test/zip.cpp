#include <catch2/catch_test_macros.hpp>
#include <iostream>
#include <ranges>

TEST_CASE("zip") {
  using namespace std::views;

  std::array<int, 4> a = {1, 2, 3, 4};
  int b[4] = {5, 6, 7, 8};

  for (auto &&[i, j] : zip(a, b)) {
    std::cout << i << " " << j << std::endl;
  }
}
